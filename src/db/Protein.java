/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package db;

import xglycscan.enums.SearchDatabaseType;

/**
 *
 * @author paiyeta1
 */
public class Protein {
    
    private String titleLine;
    private String sequence;
        
    public Protein(String titleLine, String sequence){
        this.titleLine = titleLine;
        this.sequence = sequence;
    }
        
    public String getTitleLine() {
        return titleLine;
    }

    public String getSequence() {
        return sequence;
    }
    
    public String getReversedSequence(){
        char[] reverseStringArray = new char[sequence.length()];
        for(int i = sequence.length() - 1, j = 0; i != -1; i--, j++ ){
            reverseStringArray[j] = sequence.charAt(i);
        }
        return new String(reverseStringArray);
    }

    public int getAccession(SearchDatabaseType dbtype) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int acc = 0;
        switch(dbtype){
            case IPIv387:
                 if(titleLine.startsWith("IPI")){
                    // Parse out the IPI number
                    // line = line.substring(0, 11);
                    String str = titleLine.substring(4, 15);
                    str = str.toLowerCase();
                    if(str.startsWith("ipi")) str = str.substring(3, 11);
                    else str = str.substring(0, 8);

                    // Find the most sig fig
                    int mostSigDigit = 0;
                    while(str.charAt(mostSigDigit) == '0'  &&  mostSigDigit  < 8) mostSigDigit++;
                    str = str.substring(mostSigDigit);
                    acc = Integer.parseInt(str);
                }
                break;
                
            default: //REFSEQ
                String[] lineArr = titleLine.split("\\|");
                acc = Integer.parseInt((lineArr[1]));
                break;
        }
        return acc;
    }
    
}
