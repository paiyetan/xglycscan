/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */

package db.fastadb;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import xglycscan.enums.SearchDatabaseType;

/**
 * A <code>FASTA</code> database, loaded into memory.  A <code>FastaDb</code> is essentially a linear array
 * of <code>FastaDbEntry</code> objects.
 *
 * <p>
 * (c) Institute for Systems Biology
 *
 * @author	Paul Loriaux
 * @version $Id$
 * @since	1.0
 *
 * file		$RCSFile$
 * created	Thursday, October 07, 2004
 * modified	$Date$
 */

public class FastaDb extends Object{
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
     * 		Attributes																						*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

     /**
      * The set of <code>FastaDbEntry</code> objects that comprise this <code>FastaDb</code>.
      */
    private FastaDbEntry[] entries;

    /**
     * A <code>Comparator</code> by which to sort this <code>FastaDb</code> object's entriesArr.
     */
    private Comparator c = new Comparator() {
        public int compare(Object o1, Object o2){
            try {
                return compare((FastaDbEntry)o1, (FastaDbEntry)o2);
            } catch(ClassCastException CCE) {
                return 0;
            }
        }
        public int compare(FastaDbEntry e1,FastaDbEntry e2) {
            int ipi1 = e1.getIpi();
            int ipi2 = e2.getIpi();
            if(ipi1 < ipi2) return -1;
            if(ipi1 > ipi2) return 1;
            return 0;
        }
        public boolean equals(Object obj ) {
            return obj.equals(this);
        }
    };

    private Comparator c2 = new Comparator(){
        public int compare(Object o1, Object o2) {
            try{
                return compare( (FastaDbEntry)o1, (FastaDbEntry)o2 );
            }catch( ClassCastException CCE){
                return 0;
            }
        }
        public int compare( FastaDbEntry e1, FastaDbEntry e2 ) {
            return e1.getSequence().compareTo( e2.getSequence() );
        }
        public boolean equals( Object obj ) {
            return obj.equals( this );
        }
    };

    /**
     * The <code>DecimalFormat</code> object used to return an <i>IPI</i> number as a string of eight
     * digits with leading zeroes.
     */
    private final static DecimalFormat formatter = new DecimalFormat( "00000000" );

    /**
     * A <code>FastaDbEntry</code> wrapper for a search request.
     */
    private FastaDbEntry search;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Constructor																						*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Creates a new <code>FastaDb</code> containing the given array of <code>FastaDbEntry</code> objects.
     *
     * @param entriesArr the set of <code>FastaDbEntry</code> objects comprising this <code>FastaDb</code>.
     */

    public FastaDb(FastaDbEntry[] entries){
        // Create a new Java Object
        super();

        // Sort entriesArr
        Arrays.sort(entries, c);
        this.entries = entries;
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Methods																							*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Returns the set of <code>FastaDbEntry</code> objects that comprise this <code>FastaDb</code>.
     *
     * @return the set of <code>FastaDbEntry</code> objects that comprise this <code>FastaDb</code>.
     */

    public FastaDbEntry[] getEntries(){
        return this.entries;
    }


   /**
    * Returns the <code>FastaDbEntry</code> with the specified ipi number.
    *
    * @param ipi the ipi number of the <code>FastaDbEntry</code> to be returned.
    *
    * @return the <code>FastaDbEntry</code> with the specified ipi number, or <code>null</code> if no
    * such entry is found.
    */

    public FastaDbEntry getEntry(String ipi,SearchDatabaseType dbtype){
        FastaDbEntry entry = null;

        // Create the dummy search term and search for it
        search = new FastaDbEntry(ipiToInt(ipi,dbtype), "", "", dbtype );
        int index = Arrays.binarySearch(entries, search, c);
        if( !(index < 0) ) entry = entries[ index ];
        return entry;
    }

    public FastaDbEntry[] getEntriesWhoseSequenceContains(String s){
        ArrayList entryList = new ArrayList();
        for(int i = 0; i < entries.length; i++){
            if( entries[i].getSequence().indexOf(s) >= 0 ) entryList.add( entries[i] );
        }
        FastaDbEntry[] entriesArr = new FastaDbEntry[entryList.size()];
        entryList.toArray(entriesArr);
        return entriesArr;
    }


    public FastaDbEntry[] getEntriesWhoseHeaderContains(String s){
        ArrayList entryList = new ArrayList();
        for( int i = 0; i < entries.length; i++){
            if(entries[i].getHeader().indexOf(s) >= 0) entryList.add( entries[i] );
        }
        FastaDbEntry[] entries = new FastaDbEntry[entryList.size()];
        entryList.toArray( entries );
        return entries;
    }


    /**
     * Converts the specified ipi number from its <code>String</code> representation to its <code>int
     * </code> representation.
     *
     * @param ipi the ipi number in its <code>Sting</code> representation.
     *
     * @return the specified ipi number in its <code>int</code> representation.
     */
    public static int ipiToInt(String ipi, SearchDatabaseType dbtype){
        // Parse out "IPI"
        ipi = ipi.toLowerCase();
        switch(dbtype){
            case IPIv387:
                if(ipi.startsWith("ipi")) ipi = ipi.substring(3, 11);
                else ipi = ipi.substring(0, 8);

                // Find the most sig fig
                int mostSigDigit = 0;
                while(ipi.charAt(mostSigDigit) == '0'  &&  mostSigDigit  < 8) mostSigDigit++;
                ipi = ipi.substring(mostSigDigit);
                break;
            default: //REFSEQ
                
  
                
        }
        
        return Integer.decode(ipi).intValue();
    }


    /**
        * Converts the specified ipi number from its <code>int</code> representation to its <code>String
        * </code> representation.
        *
        * @param ipi the ipi number in its <code>int</code> representation.
        *
        * @return the specified ipi number in its <code>String</code> representation.
        */

    public static String ipiToString( int ipi, SearchDatabaseType dbtype ){
        StringBuffer sb = null;
        switch(dbtype){
            case IPIv387:
                sb = new StringBuffer( "IPI" );
                sb.append( formatter.format(ipi) );
                break;
            default: //REFSEQ
                sb = new StringBuffer( "gi|" );
                sb.append( ipi );
                break;                   
        }
        return sb.toString();
    }


    /**
     * Prints the frequency of each amino acid.
     */

    public void printAaFreq(){
        DecimalFormat formatter = new DecimalFormat( "#.0000" );
        int aas = 0;
        int count[] = new int[ 91 ];
        for( int i = 0; i < count.length; i++ ) count[i] = 0;

        for( int i = 0; i < entries.length; i++ ){
            char[] seq = entries[i].getSequence().toCharArray();
            for( int j = 0; j < seq.length; j++ ){
                if( seq[j] < 65  ||  seq[j] > 90 ){
                    System.err.println( "Error in IPI or REFSEQ" + entries[i].getIpi() + ": char = '" + ((char)seq[j]) + "'" );
                }
                else{
                    aas++;
                    count[ seq[j] ]++;
                }
            }
        }

        // Print out the aa frequencies
        System.out.println( "Fasta amino acid frequencies:" );
        for( int i = 'A'; i <= 'Z'; i++ ) System.out.print( ((char)i) + "\t" );
        System.out.println( "" );

        for( int i = 'A'; i <= 'Z'; i++ )
        {
                if( count[i] == 0 ) System.out.print( "\t" );
                else
                {
                        double freq = ((double)count[i]) / ((double)aas);
                        System.out.print( formatter.format(freq) + "\t" );
                }
        }
        System.out.println( "" );
    }


   /**
    * Returns a <code>String</code> representation of this <code>FastaDb</code>.  This is simply the <code>
    * String</code> representations for the set of <code>FastaDbEntry</code> objects that comprise this
    * <code>FastaDb</code>.
    *
    * @return a <code>String</code> representation of this <code>FastaDb</code>.
    */
    public String toString()
    {
            StringBuffer sb = new StringBuffer();
            for( int i = 0; i < entries.length; i++ ) sb.append( entries[i].toString() + "\n" );
            return sb.toString();
    }

} /* end class FastaDb */