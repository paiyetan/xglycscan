/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package db.searches.psmPeptide.quan.itraq4plex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import db.searches.psmPeptide.PSMPeptide;

/**
 *
 * @author paiyeta1
 */
public class PSMPeptideDataNormalizer {
    
    private final int VALUE_114 = 1;
    private final int VALUE_115 = 2;
    private final int VALUE_116 = 3;
    private final int VALUE_117 = 4;
    
    private final int COEFF_MEAN = 1;
    private final int COEFF_MEDIAN = 2;
    
    
    public ArrayList<PSMPeptide> normalizeWithinArray(ArrayList<PSMPeptide> summarizedPeptides){
        
        ArrayList<PSMPeptide> normalized = new ArrayList<PSMPeptide>();
        
        ArrayList<Double> values_114 = getValues(summarizedPeptides,VALUE_114); //Base values
        ArrayList<Double> values_115 = getValues(summarizedPeptides,VALUE_115);
        ArrayList<Double> values_116 = getValues(summarizedPeptides,VALUE_116);
        ArrayList<Double> values_117 = getValues(summarizedPeptides,VALUE_117);
        
        double coeff114 = getCoefficient(values_114, COEFF_MEDIAN);
        double coeff115 = getCoefficient(values_115, COEFF_MEDIAN);
        double coeff116 = getCoefficient(values_116, COEFF_MEDIAN);
        double coeff117 = getCoefficient(values_117, COEFF_MEDIAN);
        
        Iterator<PSMPeptide> itr = summarizedPeptides.iterator();
        
        while(itr.hasNext()){
            PSMPeptide pep = itr.next();
            pep.normalizeChannel(VALUE_115,coeff115,coeff114);
            pep.normalizeChannel(VALUE_116,coeff116,coeff114);
            pep.normalizeChannel(VALUE_117,coeff117,coeff114);
            normalized.add(pep);
        }
        
        return normalized;
    }

    private ArrayList<Double> getValues(ArrayList<PSMPeptide> summarizedPeptides, int channel) {
        //throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<Double> values = new ArrayList<Double>();
        Iterator<PSMPeptide> itr = summarizedPeptides.iterator();
        while(itr.hasNext()){
            PSMPeptide pep = itr.next();
            
            switch(channel){
                
                case VALUE_114:
                    values.add(pep.getItraq_114());
                    break;
                case VALUE_115:
                    values.add(pep.getItraq_115());
                    break;
                case VALUE_116:
                    values.add(pep.getItraq_116());
                    break;
                case VALUE_117:
                    values.add(pep.getItraq_117());
                    break;
                default:
                    break;                    
            }
                    
        }
        
        
        return values;
    }

    private double getCoefficient(ArrayList<Double> val, int coef_type) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double[] values = toArray(val);
        double coef = 0;
        
        switch(coef_type){
            
            case COEFF_MEAN: //Mean_based
                coef = mean(values);
                break;
                
            case COEFF_MEDIAN: //Median_based
                Arrays.sort(values);
                coef = median(values);
                break;
                
            default:
                break;
        
         }       
        return coef;
        
    }

    private double[] toArray(ArrayList<Double> values) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double[] val = new double[values.size()];
        for(int i = 0; i < values.size(); i++){
            val[i] = values.get(i);
        }       
        return val;
    }
    
    public double median(double[] m) {
        
        int middle = m.length/2;
        
        if (m.length%2 == 1) {
            return m[middle];
        } else {
            return (m[middle-1] + m[middle]) / 2.0;
        }
    }
    
    public static double mean(double[] m) {
        double sum = 0;
        for (int i = 0; i < m.length; i++) {
            sum += m[i];
        }
        return sum / m.length;
    }

    
    
    
}
