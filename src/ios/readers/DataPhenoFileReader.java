/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package ios.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import xglycscan.enums.QuantificationType;

/**
 *
 * @author paiyeta1
 */
public class DataPhenoFileReader {
    
    /*
        pDatapath (file) is the path to a tab-delimited[table] file with each row representing
        the .raw or .msf file name and the names of samples in channels 114,115,116,117.
        the column headers are: spectrum_file_name | channel_114 | channel_115 | channel_116 | channel_117
        this can be interactively generated [in subsequent version implementation of program]
    * 
    */
    public HashMap<String,String> readiTRAQ4plexPhenoFile(String file){
        HashMap<String,String> file2SampleMap = new HashMap<String,String>();
        try {
            BufferedReader reader = null;
            reader = new BufferedReader(new FileReader(new File(file)));
            String line;
            int linesRead = 0;
            while((line = reader.readLine())!= null){
                linesRead++;
                if(linesRead > 1){ // skip header
                    String[] lineArr = line.split("\t");
                    // get map keys
                    String key114 = lineArr[0] + "." + "114";
                    String key115 = lineArr[0] + "." + "115";
                    String key116 = lineArr[0] + "." + "116";
                    String key117 = lineArr[0] + "." + "117";
                    // get map values
                    String value114 = lineArr[1];
                    String value115 = lineArr[2];
                    String value116 = lineArr[3];
                    String value117 = lineArr[4];
                    // insert in map object
                    file2SampleMap.put(key114, value114);
                    file2SampleMap.put(key115, value115);
                    file2SampleMap.put(key116, value116);
                    file2SampleMap.put(key117, value117);
                }

            }
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(DataPhenoFileReader.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return file2SampleMap;
    }
    
    
    public HashMap<String,String> readSpectraCountPhenoFile(String file){
        HashMap<String,String> file2SampleMap = new HashMap<String,String>();
        try {
            BufferedReader reader = null;
            reader = new BufferedReader(new FileReader(new File(file)));
            String line;
            int linesRead = 0;
            while((line = reader.readLine())!= null){
                linesRead++;
                if(linesRead > 1){ // skip header
                    String[] lineArr = line.split("\t");
                    // get map keys
                    String key1 = lineArr[0];
                    // get mapped value
                    String value1 = lineArr[1];
                    // insert in map object
                    file2SampleMap.put(key1, value1);                  
                }

            }
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(DataPhenoFileReader.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return file2SampleMap;
    }

    public boolean fileIsWellFormed(String file, QuantificationType quanType) {
        
        boolean wellformed = true;
        
        try {
            if(new File(file).exists() == false || file == null){
                wellformed = false;
                
            } else {
                BufferedReader reader = null;
            
                reader = new BufferedReader(new FileReader(new File(file)));
                String line;
                while((line = reader.readLine())!= null){

                    String[] lineArr = line.split("\t");
                    switch(quanType){
                        case SPECTRA_COUNT:
                            if(lineArr.length < 2){ 
                                wellformed = false;
                                break;
                            }
                            break;
                        case iTRAQ4plex:
                            if(lineArr.length < 5){
                                wellformed = false;
                                break;
                            }
                            break;
                    }               
                }
                reader.close();
            }
            
        } catch (IOException ex) {
            Logger.getLogger(DataPhenoFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }       
        return wellformed;  
        
    }
    
    public HashMap<String,String> autoGeneratePhenoMap(String[] files, QuantificationType quanType){
        
        HashMap<String,String> file2SampleMap = new HashMap<String,String>();
        switch(quanType){
            case iTRAQ4plex:
                for(int i = 0; i < files.length; i++){
                    String fName = new File(files[i]).getName();
                    file2SampleMap.put(fName + ".114", fName + ".114");
                    file2SampleMap.put(fName + ".115", fName + ".115");
                    file2SampleMap.put(fName + ".116", fName + ".116");
                    file2SampleMap.put(fName + ".117", fName + ".117");
                }
                break;
            case SPECTRA_COUNT:
                for(int i = 0; i < files.length; i++){
                    String fName = new File(files[i]).getName();
                    file2SampleMap.put(fName, fName);                    
                }
                break;
            default: // 
        }
        return file2SampleMap;
        
    }
    
    
    
    
}
