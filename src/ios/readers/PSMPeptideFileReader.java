/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package ios.readers;

import db.searches.psmPeptide.PSMPeptide;
import java.io.*;
import java.util.ArrayList;
import xglycscan.XGSFileAttributeIndex;

/**
 * @author paiyeta1
 *
 */
public class PSMPeptideFileReader {
	
    // this implementation assumes to be reading M2Lite-derived thermoscientific Proteome discoverer's
    public ArrayList<PSMPeptide> readAnyThermoPSMTextFile(String inputFile) throws FileNotFoundException, IOException {

        ArrayList<PSMPeptide> pDPeps = new ArrayList<PSMPeptide>();
        BufferedReader input = new BufferedReader(new FileReader(new File(inputFile)));

        int lines_read = 0;
        String line;
        XGSFileAttributeIndex[] fileAttrIndeces = null;
        int noOfColumnsLooked = 0;
        int psmRowIndex = 0;
        while ((line = input.readLine()) != null) {
            lines_read++;
            if (lines_read == 1){
                // get the attributes reported for input file
                String[] lineContent = line.split("\t");
                fileAttrIndeces = new XGSFileAttributeIndex[lineContent.length];
                // get the indeces for the reported attributes
                for (int i = 0; i < lineContent.length; i++){
                    fileAttrIndeces[i] = new XGSFileAttributeIndex(unquote(lineContent[i]),i); 
                }
                noOfColumnsLooked = fileAttrIndeces.length - 1;//not interested in the Annnotation column in this implementation 
            }
            if(lines_read != 1){
                psmRowIndex++;
                String[] lineContent = line.split("\\t");
                if (lineContent.length >= noOfColumnsLooked){
                    // attributes we're interested in 
                    // for each attribute, get representative index and extract from such in lineContent
                    String conf = unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Confidence Level")]); //peptide confidence Confidence Level
                    String sequence = unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Sequence")]);	
                    //String protDescr = unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Protein Descriptions")]);// = unquote(lineContent[6]); //protein description	
                    //int proteins = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"# Proteins")])); //no of proteins mapped to	
                    //int proteinGrps = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"# Protein Groups")])); //no of protein groups mapped to
                    //String[] protGrpAccns = setProtGrpAccns(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Protein Group Accessions")]));
                    String[] modifxns = setModifications(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Modifications")]));	
                    //double deltaScore = Double.valueOf(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"DeltaScore")]));	
                    //int rank = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Rank")]));	
                    int sEngineRank = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Search Engine Rank")]));
                    double itraq_114 = Double.valueOf(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"114")])));	
                    double itraq_115 = Double.valueOf(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"115")])));	
                    double itraq_116 = Double.valueOf(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"116")])));	
                    double itraq_117 = Double.valueOf(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"117")])));	
                    double xCorr = Double.valueOf(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"XCorr")])));	
                    double spScore = Double.parseDouble(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"SpScore")])));
                    int mCleavs = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Missed Cleavages")])); //missed cleavages	
                    double intensity = Double.valueOf(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Intensity")])));	
                    int charge = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Charge")]));	
                    double mz = calcMZ(Double.parseDouble(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Mass")]))),charge); // [Da]	
                    double rTime = Double.parseDouble(test(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"RT [min]")]))); //[min] retention time	
                    int matIons = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Matched Ions")])); //matched ions	
                    int totIons = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Total Ions")])); //total ions	
                    String specFile = 
                            new File(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"Spectrum File")])).getName(); //spectrum file	

                    //String proteinID = protGrpAccns[0];
                    //String formatted_seq = formatter.formatPeptide(sequence.toUpperCase(), proteinID, db);

                    double evaluationValue = getEvaluationValue(conf);
                    int thermoPDPeptideConfidence = Integer.parseInt(conf); 
                    
                    //"First Scan"
                    int firstScan = Integer.parseInt(unquote(lineContent[getAttributeIndex(fileAttrIndeces,"First Scan")]));
                    
                    if(thermoPDPeptideConfidence > 1){ //defaults to greater than Low (assuming low is anything more than 0.05 FDR                          
                        PSMPeptide psmPep = new PSMPeptide(new StringBuilder().append(specFile).append("_").append(psmRowIndex).toString(), 
                                                        sequence, 
                                                        modifxns, 
                                                        sEngineRank,
                                                        itraq_114, 
                                                        itraq_115, 
                                                        itraq_116, 
                                                        itraq_117,
                                                        xCorr,
                                                        spScore, 
                                                        mCleavs, 
                                                        intensity, 
                                                        charge, 
                                                        mz, 
                                                        rTime, 
                                                        matIons, 
                                                        totIons,
                                                        specFile,
                                                        evaluationValue,
                                                        firstScan);
                        pDPeps.add(psmPep);
                    }
                }
            }
        }
        System.out.println("   " + pDPeps.size() + " \"ThermoPDPeptide\" objects [psm peptides] found in file to pass filter...");
        return pDPeps;
    }
    
    
    public ArrayList<PSMPeptide> readPSMPeptideListFile(String inputFile) throws FileNotFoundException, IOException {

        ArrayList<PSMPeptide> psmPeps = new ArrayList<PSMPeptide>();
        BufferedReader reader = new BufferedReader(new FileReader(new File(inputFile)));
        String line;
        int psmRowIndex = 0;
        String specFile = new File(inputFile).getName();
        while((line = reader.readLine())!=null){
            psmRowIndex++;
            String sequence = line.replaceAll("[^A-Z]", "");
            PSMPeptide psmPep = new PSMPeptide(new StringBuilder().append(specFile).append("_").append(psmRowIndex).toString(), 
                                                        sequence, 
                                                        // substitute other parameters with dummy values...
                                                        setModifications("XxX;xxx;xxxX"), //modifications
                                                        1, //search Engine Rank
                                                        0.0000,  // itraq4plex 114
                                                        0.0000, // itraq4plex 115
                                                        0.0000, // itraq4plex 116
                                                        0.0000, // itraq4plex 117
                                                        2.0000, // XCorr
                                                        2.0000, //spScore
                                                        1, //mCleavs - missed cleavages
                                                        0.0000, // intensity
                                                        1,  // charge
                                                        2.0000, // mz
                                                        2.0000, // retention time
                                                        10, // matched Ions
                                                        20, // total Ions
                                                        specFile, // spectrum file (in this case, the peptide list file
                                                        0.9000, // evaluation value
                                                        123); // first scan
            psmPeps.add(psmPep);
        }   
        return psmPeps;
    }
    



    /** 
    * This String util method removes single or double quotes 
    * from a string if its quoted. 
    * for input string = "mystr1" output will be = mystr1 
    * for input string = 'mystr2' output will be = mystr2 
    * 
    * @param String value to be unquoted. 
    * @return value unquoted, null if input is null. 
    * 
    */  
    public String unquote(String s) {  
        if (s != null && ((s.startsWith("\"") && s.endsWith("\"")) || 
                (s.startsWith("'") && s.endsWith("'")))) {  
            s = s.substring(1, s.length() - 1);  
        }  
        return s;  
    }  

    /*
    private String[] setProtGrpAccns(String string) {
            String[] grpAccns = string.split(";");
            return grpAccns;
    }
    * 
    */

    private String[] setModifications(String string) {
            String[] modfxns = string.split("; ");
            return modfxns;
    }

    

    private int getAttributeIndex(XGSFileAttributeIndex[] fileAttrIndeces, String attr) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int attrIndex = 0;
        for(int i = 0; i < fileAttrIndeces.length; i++){
            if(attr.equalsIgnoreCase(fileAttrIndeces[i].getAttribute())){
                attrIndex = i;
            }
        }       
        return attrIndex;
    }
    
    private String test(String str){
        String s = "0";
        if(str!=null && !str.isEmpty()){
            s = str;
        }
        return s;
    }

    private double calcMZ(double mass, int charge) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double mz = 0;
        //mz = [mass + massOfcharge(charge)]/charge
        mz =  (mass + (1.007825 * ((double) charge)))/(double) charge;
        return mz;
    }

    private double getEvaluationValue(String conf) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double evalValue = 0.00;
        if(conf.equalsIgnoreCase("3")){
            evalValue = 0.99;
        } else {
            evalValue = 0.95;
        }
        
        return evalValue;
    }

}
