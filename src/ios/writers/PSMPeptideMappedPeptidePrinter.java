/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package ios.writers;

import db.searches.psmPeptide.PSMPeptide;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import xglycscan.enums.QuantificationType;

/**
 *
 * @author paiyeta1
 */
public class PSMPeptideMappedPeptidePrinter {
    
    
    public void print(String output, 
                        ArrayList<PSMPeptide> mappedPDPeps, 
                            QuantificationType quanType){
        
        Iterator<PSMPeptide> itr = mappedPDPeps.iterator();
        try {
            
            PrintWriter writer = new PrintWriter(output);
            //print header
            switch (quanType){
                case iTRAQ4plex:
                    //write tab-delimiited file header
                    writer.println("PeptideID" + "\t" + 
                                   "Sequence" + "\t" +

                                   // --- only in Glycosite in Mapped PSMPeptide --- //
                                   "Identified Glycosite" + "\t" +
                                   // --------------------------------------------------- //
                                   "Modifications" + "\t" +
                                    "Search Engine Rank" + "\t" +
                                    //"114" + "\t" +
                                    //"115" + "\t" +
                                    //"116" + "\t" +
                                    //"117" + "\t" +
                                    "XCorr" + "\t" +
                                    //"SpScore" + "\t" +
                                    "Missed Cleavage" + "\t" +
                                    "Intensity" + "\t" +
                                    "Charge" + "\t" +
                                    "m/z [Da]" + "\t" +
                                    "RT [min]" + "\t" +
                                    "Matched Ions" + "\t" +
                                    "Total Ions" + "\t" +
                                    "Spectrum File");
                    // write rest of the data...
                    while(itr.hasNext()){
                        PSMPeptide pep = itr.next();
                        writer.println(pep.getPeptideID() + "\t" + 
                                        pep.getSequence() + "\t" +
                                        pep.getGlycosite().getSequence() + "\t" +
                                        toString(pep.getModifxns()) + "\t" +
                                        
                                        pep.getsEngineRank() + "\t" +
                                        //pep.getItraq_114() + "\t" +
                                        //pep.getItraq_115() + "\t" +
                                        //pep.getItraq_116() + "\t" +
                                        //pep.getItraq_117() + "\t" +
                                        pep.getxCorr() + "\t" +
                                        //pep.getSpScore() + "\t" +
                                        pep.getmCleavs() + "\t" +
                                        pep.getIntensity() + "\t" +
                                        pep.getCharge() + "\t" +
                                        pep.getMz() + "\t" +
                                        pep.getrTime() + "\t" +
                                        pep.getMatIons() + "\t" +
                                        pep.getTotIons() + "\t" +
                                        pep.getSpecFile().replace(".mzid", "")
                                    );
                    }
                    break;
                
                default: // SPECTRA_COUNT
                    //write tab-delimiited file header
                    writer.println("PeptideID" + "\t" + 
                                   "Sequence" + "\t" +

                                   // --- only in Glycosite in Mapped thermoPDPeptide --- //
                                   "Identified Glycosite" + "\t" +
                                   // --------------------------------------------------- //

                                    //"Protein Descriptions" + "\t" +
                                    //"# Proteins" + "\t" + 
                                    //"# Protein Groups" + "\t" +
                                    //"Protein Group Accessions" + "\t" +
                                    "Modifications" + "\t" +
                                    //"DeltaScore" + "\t" +
                                    //"Rank" + "\t" +
                                    "Search Engine Rank" + "\t" +
                                    //"114" + "\t" +
                                    //"115" + "\t" +
                                    //"116" + "\t" +
                                    //"117" + "\t" +
                                    //"XCorr" + "\t" +
                                    //"SpScore" + "\t" +
                                    //"Missed Cleavage" + "\t" +
                                    //"Intensity" + "\t" +
                                    "Charge" + "\t" +
                                    "m/z [Da]" + "\t" +
                                    //"RT [min]" + "\t" +
                                    //"Matched Ions" + "\t" +
                                    //"Total Ions" + "\t" +
                                    "Scan Number" + "\t" +
                                    "Spectrum File");
                    // write rest of the data...
                    while(itr.hasNext()){
                        PSMPeptide pep = itr.next();
                        writer.println(pep.getPeptideID() + "\t" + 
                                        pep.getSequence() + "\t" +
                                        pep.getGlycosite().getSequence() + "\t" +
                                        toString(pep.getModifxns()) + "\t" +
                                        
                                        pep.getsEngineRank() + "\t" +
                                        //pep.getItraq_114() + "\t" +
                                        //pep.getItraq_115() + "\t" +
                                        //pep.getItraq_116() + "\t" +
                                        //pep.getItraq_117() + "\t" +
                                        //pep.getxCorr() + "\t" +
                                        //pep.getSpScore() + "\t" +
                                        //pep.getmCleavs() + "\t" +
                                       // pep.getIntensity() + "\t" +
                                        pep.getCharge() + "\t" +
                                        pep.getMz() + "\t" +
                                        //pep.getrTime() + "\t" +
                                        //pep.getMatIons() + "\t" +
                                        //pep.getTotIons() + "\t" +
                                        pep.getScanNumber() + "\t" +
                                        pep.getSpecFile().replace(".mzid", "")
                                    );
                    }
                    break;
                    
            }       
            writer.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PSMPeptideMappedPeptidePrinter.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    private String toString(String[] strArray) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String str = "";
        if(strArray.length > 0){
            str = strArray[0];
            for(int i = 1; i < strArray.length; i++){
                str = str + "; " + strArray[i];
            }
        }
        return str;
    }
    
    
}
