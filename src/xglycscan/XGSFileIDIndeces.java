/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import db.searches.psmPeptide.PSMPeptide;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author paiyeta1
 */
public class XGSFileIDIndeces {
    
    
    
    private int numberOfIDPeptides;
    private int numberOfUniqIDPeptides;
    private int numberOfIDGlycosites;
    private int numberOfUniqIDGlycosites;
    private double specificity;
    
    private ArrayList<String> iDPeps;
    private ArrayList<String> iDGlycs;
   
    private ArrayList<String> iDUniqPeps;
    private ArrayList<String> iDUniqGlycs;
    private String fileName;
    
    
    /* 
     * Extract the specific file identification indeces
     * the file identification indeces are described as 
     * 1. the total number of identified peptides
     * 2. the total number of unique identified peptides
     * 3. the total number of Glycosites
     * 4. the total number of unique Glycosites
     */ 
    public XGSFileIDIndeces(){
        numberOfIDPeptides = 0;
        numberOfUniqIDPeptides = 0;
        numberOfIDGlycosites = 0;
        numberOfUniqIDGlycosites = 0;
        
        iDPeps = new ArrayList<String>();
        iDGlycs = new ArrayList<String>();
   
        iDUniqPeps = new ArrayList<String>();
        iDUniqGlycs = new ArrayList<String>();
        specificity = 0.0;
        //extractFileIDIndeces(mappedXTandemPeps);
    }

    public void extractXGSFileIDIndeces(String fileName, ArrayList<PSMPeptide> mappedPDPeps) {
        //throw new UnsupportedOperationException("Not yet implemented");
        Iterator<PSMPeptide> itr = mappedPDPeps.iterator();
        while(itr.hasNext()){
            PSMPeptide pep = itr.next();
            numberOfIDPeptides++;
            String peptide_sequence = pep.getSequence();
            iDPeps.add(peptide_sequence);
            if(inUniqIDPeps(peptide_sequence)==false){
                iDUniqPeps.add(peptide_sequence);
            }
            if(pep.getGlycosite().getSequence().equalsIgnoreCase("Not_Found")==false){
                numberOfIDGlycosites++;
                String glyc_sequence = pep.getGlycosite().getSequence(); 
                iDGlycs.add(glyc_sequence);
                if(inUniqIDGlyc(glyc_sequence)==false){
                    iDUniqGlycs.add(glyc_sequence);
                } // end if 
            } // end if 
        } // end while
        this.fileName = fileName; //usually the full path to file...
        numberOfUniqIDPeptides = iDUniqPeps.size();
        numberOfUniqIDGlycosites = iDUniqGlycs.size();
        calcSpecificity();
    }

    private boolean inUniqIDPeps(String peptide_sequence) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in = false;        
        Iterator<String> itr = iDUniqPeps.iterator();
        while(itr.hasNext()){
            String seq = itr.next();
            if(seq.equalsIgnoreCase(peptide_sequence)){
                in = true;
                break;
            }
        }
        return in;
    } //

    private boolean inUniqIDGlyc(String glyc_sequence) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in = false;
        Iterator<String> itr = iDUniqGlycs.iterator();
        while(itr.hasNext()){
            String seq = itr.next();
            if(seq.equalsIgnoreCase(glyc_sequence)){
                in = true;
                break;
            }
        }
        return in;
    }

    public String getFileName() {
        return fileName;
    }
    
    public ArrayList<String> getiDUniqGlycs() {
        return iDUniqGlycs;
    }

    public ArrayList<String> getiDUniqPeps() {
        return iDUniqPeps;
    }

    public ArrayList<String> getiDGlycs() {
        return iDGlycs;
    }

    public ArrayList<String> getiDPeps() {
        return iDPeps;
    }
    
    public int getNumberOfIDGlycosites() {
        return numberOfIDGlycosites;
    }

    public int getNumberOfIDPeptides() {
        return numberOfIDPeptides;
    }

    public int getNumberOfUniqIDGlycosites() {
        return numberOfUniqIDGlycosites;
    }

    public int getNumberOfUniqIDPeptides() {
        return numberOfUniqIDPeptides;
    }

    private void calcSpecificity() {
        //throw new UnsupportedOperationException("Not yet implemented");
        double noPSMs = (double) numberOfIDPeptides;
        double noGlycs = (double) numberOfIDGlycosites;
        specificity =  noGlycs/noPSMs;
    }
    
    public double getSpecificity(){
        return specificity;
    }

    

    
    
    
    
}
