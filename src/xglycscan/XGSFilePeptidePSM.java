/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

/**
 *
 * @author paiyeta1
 */
public class XGSFilePeptidePSM {
    
    
    
    private String fileName;
    private int fileRowIndex;
    private String exactIDSequence; //identified peptide clean_sequence
    private String extendedSequence; //formatted sequence
    private double probability; // the evaluation value
    private XGSGlycosite idGlycosite = null;
    
    //additional values for mapping
    private double rT; //for validation purposes of scanNumber mapping
    private double mz; //for validation purposes of scanNnumber mapping
    private int charge; //for validation purposes of scanNumber mapping
    
    private int scanNumber;
    
    /*
     * public XGSFilePeptidePSM(String peptideID, int fileRowIndex, String sequence, String extendedSequence, 
                            double probability//,
                            //double rT,
                            //double mz,
                            //int charge,
                            //int scanNumber
                            ) {
        this.fileName = peptideID; // supposedly a unique identifier, was spectrum_loc in previous, but in this case peptideID
        this.fileRowIndex = fileRowIndex;
        this.exactIDSequence = sequence;
        this.extendedSequence = extendedSequence;
        this.probability = probability;               
    }
    * 
    */

    public XGSFilePeptidePSM(String peptideID, 
                                int fileRowIndex, 
                                    String exactIDSequence, 
                                        String extendedSequence, 
                                            double probability, 
                                                double rT, double mz, int charge, int scanNumber) {
        this.fileName = peptideID;
        this.fileRowIndex = fileRowIndex;
        this.exactIDSequence = exactIDSequence;
        this.extendedSequence = extendedSequence;
        this.probability = probability;
        this.rT = rT;
        this.mz = mz;
        this.charge = charge;
        this.scanNumber = scanNumber;
    }

    public XGSFilePeptidePSM(String peptideID, 
                                int fileRowIndex, 
                                    String exactSequence, 
                                        String extendedSequence, 
                                            double probability,
                                                double rT, double mz, int charge, int scanNumber,
                                                    XGSGlycosite glycosite) {       
        this(peptideID,
                fileRowIndex,
                    exactSequence,
                        extendedSequence,
                            probability,
                                rT, mz, charge, scanNumber);
        this.idGlycosite = glycosite;
        
    }
    
    public String getFileName() {
        return fileName;
    }

    public int getFileRowIndex() {
        return fileRowIndex;
    }

    public String getExtendedSequence() {
        return extendedSequence;
    }

    public double getProbability() {
        return probability;
    }

    public String getExactIdSequence() {
        return exactIDSequence;
    }

    public XGSGlycosite getIdGlycosite() {
        return idGlycosite;
    }

    public int getCharge() {
        return charge;
    }

    public String getExactIDSequence() {
        return exactIDSequence;
    }

    public double getMz() {
        return mz;
    }

    public double getrT() {
        return rT;
    }

    public int getScanNumber() {
        return scanNumber;
    }
    
    

    public void setIdGlycosite(XGSGlycosite idGlycosite) {
        this.idGlycosite = idGlycosite;
    }
    
    
  
}
