/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class XGSGlycosite {
    
    private String sequence;
    private double probability;
    private String proteinAccn;
    private int location;
    private int numTrypticEnds;
    private double mass;
    
    
    private LinkedList<String> proteinAccns = new LinkedList<String>();
    

    public XGSGlycosite(String sequence, double probability, 
                        String proteinAccn, int location,
                            int numTrypticEnds, double mass) {
        this.sequence = sequence;
        this.probability = probability;
        this.proteinAccn = proteinAccn;
        this.location = location;
        this.numTrypticEnds = numTrypticEnds;
        this.mass = mass;
    }
    
    public XGSGlycosite(String sequence, double probability, 
                        String proteinAccn, int location,
                            int numTrypticEnds, double mass,
                                LinkedList<String> proteinAccns) {
        this(sequence, probability, proteinAccn, location, numTrypticEnds, mass);
        this.proteinAccns = proteinAccns;
    }
    
    public double getProbability() {
        return probability;
    }

    public String getProteinAccn() {
        return proteinAccn;
    }

    public String getSequence() {
        return sequence;
    }

    public int getLocation() {
        return location;
    }

    public double getMass() {
        return mass;
    }

    public int getNumTrypticEnds() {
        return numTrypticEnds;
    }

    public LinkedList<String> getProteinAccns() {
        return proteinAccns;
    }
   
    // ************ Setters ************** //
    public void setProteinAccns(LinkedList<String> proteinAccns){
        this.proteinAccns = proteinAccns;
    }
    
    public void addProteinAccn(String accn){
        proteinAccns.add(accn);
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public void setNumTrypticEnds(int numTrypticEnds) {
        this.numTrypticEnds = numTrypticEnds;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public void setProteinAccn(String proteinAccn) {
        this.proteinAccn = proteinAccn;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    
    

}
