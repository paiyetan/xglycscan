/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import db.searches.psmPeptide.PSMPeptide;
import ios.writers.MZIDPSMPeptidesWithValuesPrinter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import uk.ac.ebi.jmzidml.model.mzidml.Peptide;
import uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidence;
import uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem;
import xglycscan.enums.EvaluationValueType;

/**
 *
 * @author paiyeta1
 */
public class XGSMZIDPSMFDRCalculator {
    
    // Class-derived properties.
    private int trueIDs;
    private int decoyIDs;
    private ArrayList<PSMPeptide> psmPeps;
   
    
    // Class instance (constructor-derived) properties...
    private String specFile; 
    private double fDRFilter;
    private LinkedList<SpectrumIdentificationItem> topRankedSIItems; 
    private HashMap<String, Peptide> pepID2PeptideMap; 
    private HashMap<String, LinkedList<PeptideEvidence>> pepID2EvidencesMap;
    private EvaluationValueType evalueType;
    private String outTableDir;
    
    private HashMap<String,String> siiId2sirIdMap;
    
    public XGSMZIDPSMFDRCalculator() {
    }

    public XGSMZIDPSMFDRCalculator(String specFile, 
                                    double fDRFilter,
                                        LinkedList<SpectrumIdentificationItem> topRankedSIItems, 
                                            HashMap<String, Peptide> pepID2PeptideMap, 
                                                HashMap<String, LinkedList<PeptideEvidence>> pepID2EvidencesMap,
                                                    EvaluationValueType evalueType,
                                                        String outTableDir,
                                                        HashMap<String,String> siiId2sirIdMap) throws FileNotFoundException {
        this.specFile = specFile;
        this.fDRFilter = fDRFilter;
        this.topRankedSIItems = topRankedSIItems;
        this.pepID2PeptideMap = pepID2PeptideMap;
        this.pepID2EvidencesMap = pepID2EvidencesMap;
        this.evalueType = evalueType;
        this.outTableDir = outTableDir;
        
        this.siiId2sirIdMap = siiId2sirIdMap;
        trueIDs = 0;
        decoyIDs = 0;
        // compute FDR....
        /*
         * Rank all top-ranked SpectrumIdentificationItems "PSMPeptide" by EvaluationValueType
         * Compute expectation value (p-value) 
         * Get Peptide (PSM) reference of all respective top-ranked SpectrumIdentificationItems
         * 
         * 
         */
        //set PSMPeptide objects
        setPSMPeptides();// all PSMPeptides (target and decoy)
        setPSMPeptidesDecoyValue();
        //setPSMPeptidesEvalValue(); - already set while instantiating peptide
        rankPSMPeptides(); //rank by Evaluation value
        setPSMPeptidesPValue();
        setPSMPeptidesFDR();
        setPSMPeptidesQValue();
        outputPSMPeptidesWithValues(); //for debugging purposes...
        // +/- apply filter to select peptides that go on to downstream analysis
        filterPSMPeptidesByFDR();
    }

    private void setPSMPeptides() {
        psmPeps = new ArrayList<PSMPeptide>();
        XGSMZIDPSMPeptideExtractor psmExtractor = new XGSMZIDPSMPeptideExtractor();
        for(SpectrumIdentificationItem topRankedItem : topRankedSIItems){
            PSMPeptide psm = psmExtractor.getPeptide(specFile, 
                                                        topRankedItem, 
                                                        pepID2PeptideMap, 
                                                        pepID2EvidencesMap, 
                                                        evalueType,
                                                        siiId2sirIdMap);
            psmPeps.add(psm);
        }
        System.out.println("   " + psmPeps.size() + " PSMPeptides are found...");
    }
    
    private void setPSMPeptidesDecoyValue() {
        
        outer: 
        for(PSMPeptide psm : psmPeps){
            
            //get peptide ID, 
            //get mapped PeptideEvidences
            //if any of the mapped PeptideEvidences setAsDecoy, then peptide is set as decoy
            String peptideID = psm.getPeptideID().replace(specFile + "_", ""); //PEP_XXXXX
            LinkedList<PeptideEvidence> mappedPeptideEvidences = pepID2EvidencesMap.get(peptideID);
            //if(mappedPeptideEvidences != null){
                for(PeptideEvidence pepEvidence : mappedPeptideEvidences){
                    if(pepEvidence.isIsDecoy()){
                        psm.setAsDecoy(); //
                        decoyIDs++;
                        continue outer;
                    }
                }
            //}
            
        }
        System.out.println("   " + decoyIDs + " decoy PSMPeptides are found...");
        System.out.println("   " + (psmPeps.size() - decoyIDs) + " target PSMPeptides are found...");
    }
    
    private void rankPSMPeptides() { //sort PSMPeptides in descending order by EvaluationValue [default: SEQUEST:xcorr]
        Collections.sort(psmPeps);
    }

    private void setPSMPeptidesPValue() {
        System.out.println("   Estimating PSMPeptides' p-values...");
        int decoys = 0;
        for(PSMPeptide pep : psmPeps){
            //count the number of decoy psm peptides with equal or higher EvaluationValue [pretty much sorted higher]
            if(pep.isIsDecoy())
                decoys++;
            double pepPValue = (double)decoys/(double)decoyIDs;
            pep.setPValue(pepPValue);          
        }
    }

    private void setPSMPeptidesFDR() { //FDR, decoys/targets at and above a cutoff EValuationValue
        System.out.println("   Estimating PSMPeptides' FDR [Elias and Gygi, 2007]...");
        int decoys = 0;
        int targets = 0;
        for(PSMPeptide pep : psmPeps){
            //count the number of decoy psm peptides with equal or higher EvaluationValue [pretty much sorted higher]
            if(pep.isIsDecoy()){
                decoys++;
            } else {
                targets++;
            }
            double fDR = (double)(decoys * 2)/(double)(decoys + targets); //Elias and Gygi, 2007.
            pep.setFDR(fDR);          
        }
    }
    
    private void setPSMPeptidesQValue() {
        System.out.println("   Estimating PSMPeptides' QValue(s)...");
        //start from the last peptide till you get to a decoy peptide
        int i = (psmPeps.size()-1);
        //for(int i = (psmPeps.size()-1); i >= 0; i--){
        while(i >= 0){     
            ArrayList<Double> fdrs = new ArrayList<Double>();
            fdrs.add(psmPeps.get(i).getfDR());
            if(psmPeps.get(i).isIsDecoy()){
                double minFDR = psmPeps.get(i).getfDR();
                psmPeps.get(i).setQValue(minFDR);
                i--;
            } else {
                int j = i - 1;
                while(j >= 0 && psmPeps.get(j).isIsDecoy()==false){
                    fdrs.add(psmPeps.get(j).getfDR());
                    j--;
                }
                double minFDR = Collections.min(fdrs);
                for(int k = (j + 1); k <= i; k++){
                    //set QValues as minFDR
                    psmPeps.get(k).setQValue(minFDR);
                }
                i = j;
            }           
        }      
    }
    
    private void outputPSMPeptidesWithValues() throws FileNotFoundException {
        System.out.println("   Printing \".mzid\" derived PSMPeptides' computed values...");
        String psmPeptideNValueDir  = outTableDir + File.separator + "values";
        if(new File(psmPeptideNValueDir).exists()==false){
            new File(psmPeptideNValueDir).mkdirs();
        }
        String filePSMValueOutputTable = psmPeptideNValueDir + File.separator + specFile.replace(".mzid", ".values");
        MZIDPSMPeptidesWithValuesPrinter printer = new MZIDPSMPeptidesWithValuesPrinter();
        printer.outputMZIDPSMPeptideValues(filePSMValueOutputTable, psmPeps);
    }
   
    private void filterPSMPeptidesByFDR() { //removes decoys and PSMPeptides with FDR (qValue) < fDRFilter
        System.out.println("   Filtering identified PSMPeptides...");
        for(int i = 0; i < psmPeps.size(); i++){
            if(psmPeps.get(i).isIsDecoy() || psmPeps.get(i).getqValue() > fDRFilter){
                psmPeps.remove(i);
                i = i - 1;
            }
        }
    }
    
    
    public ArrayList<PSMPeptide> getPsmPeps() {
        return psmPeps;
    }

    public int getDecoyIDs() {
        return decoyIDs;
    }

    public int getTrueIDs() {
        return trueIDs;
    }
    
    public int getTotalIDs(){
        return psmPeps.size();
    }

    
    
    
}
