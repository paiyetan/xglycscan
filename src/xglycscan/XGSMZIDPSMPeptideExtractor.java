/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import db.searches.psmPeptide.PSMPeptide;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import uk.ac.ebi.jmzidml.model.mzidml.Modification;
import uk.ac.ebi.jmzidml.model.mzidml.Peptide;
import uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidence;
import uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem;
import xglycscan.enums.EvaluationValueType;

/**
 *
 * @author paiyeta1
 */
public class XGSMZIDPSMPeptideExtractor {
    
    public PSMPeptide getPeptide(String specFile, 
                                    SpectrumIdentificationItem sii, 
                                        HashMap<String,Peptide> pepID2PeptideMap,
                                            HashMap<String, LinkedList<PeptideEvidence>> pepID2EvidencesMap,
                                                EvaluationValueType evalueType,
                                                    HashMap<String,String> siiId2sirIdMap){
        Peptide peptide = pepID2PeptideMap.get(sii.getPeptideRef());
        String peptideID = pepID2PeptideMap.get(sii.getPeptideRef()).getId();
        List modifications = peptide.getModification();
        String[] modifxns = convertModifications2Array(modifications);
        String sequence = peptide.getPeptideSequence();
        int sEngineRank = sii.getRank();
        int charge = sii.getChargeState();
        double mz = sii.getExperimentalMassToCharge();

        double itraq_114 = 0;	
        double itraq_115 = 0;	
        double itraq_116 = 0;	
        double itraq_117 = 0;	
        double xCorr = 0;	
        double spScore = 0;
        int mCleavs = 0; //missed cleavages	
        double intensity = 0;	
        double rTime = 0; //[min] retention time	
        int matIons = 0; //matched ions	
        int totIons = 0; //total ions	

        String psmPeptideID = specFile + "_" + peptideID;

        XGSMZIDEvalValueExtractor eValExtractor = new XGSMZIDEvalValueExtractor();
        double evaluationValue = eValExtractor.getEvaluationValue(sii,evalueType);
        
        //get scan number (spectra_nativeID)
        String sirId = siiId2sirIdMap.get(sii.getId());
        int fscan = Integer.parseInt(sirId.replace("controllerType=0 controllerNumber=1 scan=", ""));  

        PSMPeptide psmPeptide = new PSMPeptide(psmPeptideID, 
                                    sequence, 
                                    modifxns, 
                                    sEngineRank,
                                    itraq_114, 
                                    itraq_115, 
                                    itraq_116, 
                                    itraq_117,
                                    xCorr,
                                    spScore, 
                                    mCleavs, 
                                    intensity, 
                                    charge, 
                                    mz, 
                                    rTime, 
                                    matIons, 
                                    totIons,
                                    specFile,
                                    evaluationValue,
                                    fscan);
        
        return(psmPeptide);

    }
    
    private String[] convertModifications2Array(List modifications) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String[] modfxn = new String[modifications.size()];
        
        for(int i = 0; i < modfxn.length; i++){
            Modification mod = (Modification) modifications.get(i);
            int location = mod.getLocation();
            double miDeltaMass = mod.getMonoisotopicMassDelta();
            modfxn[i] = location + "[" + miDeltaMass + "]";            
        }
       
        return modfxn;
    }

    
}
