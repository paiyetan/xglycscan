/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import db.Database;
import db.Protein;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author paiyeta1
 * @date June 25, 2013
 * 
 * 
 * 
 */
public class XGSPeptideSequenceFormatter { 
    
    private String crap = new String( "[^A-Z]" );
    /*
     *  
     * // format peptide
		// search db protein headerlines for peptide associated ipi_accession
		// if (protein headerline contains accession)
			// get protein sequence,
			// extract pre-tryptic and post-tryptic residue from sequence
			// prepend pre and post-tryptic residues to peptide 
			// format derived peptide sequence
			// return formatted peptide sequence 
     *
     */
    public String formatPeptide(String pep, Database db){
        String formatted_seq = "un-assigned";
        String peptide = cleanSeq(pep); //remove craps [subsequent from varying search engine peptide annotation]
        ArrayList<Protein> proteins = db.getProteins();
        //Iterator<Protein> itr = proteins.iterator();
        /*
        while(itr.hasNext()){
            Protein protein = itr.next();
            if(protein.getTitleLine().contains(protein_acc)){
                String protein_seq = protein.getSequence();
                int peptideStartIndex = protein_seq.indexOf(peptide);
                int prePeptideTrypticIndex = peptideStartIndex - 1;
                int postPeptideTrypticIndex = peptideStartIndex + peptide.length();
                char prePeptideTrypticChar;
                char postPeptideTrypticChar;
                if(prePeptideTrypticIndex >= 0){
                    prePeptideTrypticChar = protein_seq.charAt(prePeptideTrypticIndex);
                }else{
                    prePeptideTrypticChar = '-';
                }
                
                if(postPeptideTrypticIndex < protein_seq.length()){
                    postPeptideTrypticChar = protein_seq.charAt(postPeptideTrypticIndex);
                }else{
                    postPeptideTrypticChar = '-';
                }
                formatted_seq = String.valueOf(prePeptideTrypticChar) + "." +
                        peptide + "." + String.valueOf(postPeptideTrypticChar);
                break;
            }
        }
        * 
        */
        
        if(formatted_seq.equalsIgnoreCase("un-assigned")){ // implies that peptide could not be found by accession
            // search by protein sequence
            Iterator<Protein> itr2 = proteins.iterator();
            while(itr2.hasNext()){
                Protein protein = itr2.next();
                if(protein.getSequence().contains(peptide)){
                    String protein_seq = protein.getSequence();
                    int peptideStartIndex = protein_seq.indexOf(peptide);
                    int prePeptideTrypticIndex = peptideStartIndex - 1;
                    int postPeptideTrypticIndex = peptideStartIndex + peptide.length();
                    char prePeptideTrypticChar;
                    char postPeptideTrypticChar;
                    if(prePeptideTrypticIndex >= 0){
                        prePeptideTrypticChar = protein_seq.charAt(prePeptideTrypticIndex);
                    }else{
                        prePeptideTrypticChar = '-';
                    }
                
                    if(postPeptideTrypticIndex < protein_seq.length()){
                        postPeptideTrypticChar = protein_seq.charAt(postPeptideTrypticIndex);
                    }else{
                        postPeptideTrypticChar = '-';
                    }
                    formatted_seq = String.valueOf(prePeptideTrypticChar) + "." +
                            peptide + "." + String.valueOf(postPeptideTrypticChar);
                    break;
                }
            }           
        }
        
        if(formatted_seq.equalsIgnoreCase("un-assigned")){ // still couldn't find peptides by protein sequence search
            formatted_seq =  "-." + peptide + ".-" ;
        } 
                
        return formatted_seq;
    }
    
    public String formatPeptide(String pep, String protein_acc, Database db){
        String formatted_seq = "un-assigned";
        String peptide = cleanSeq(pep); //remove craps [subsequent from varying search engine peptide annotation]
        ArrayList<Protein> proteins = db.getProteins();
        Iterator<Protein> itr = proteins.iterator();
        
        while(itr.hasNext()){
            Protein protein = itr.next();
            if(protein.getTitleLine().contains(protein_acc)){
                String protein_seq = protein.getSequence();
                int peptideStartIndex = protein_seq.indexOf(peptide);
                int prePeptideTrypticIndex = peptideStartIndex - 1;
                int postPeptideTrypticIndex = peptideStartIndex + peptide.length();
                char prePeptideTrypticChar;
                char postPeptideTrypticChar;
                if(prePeptideTrypticIndex >= 0){
                    prePeptideTrypticChar = protein_seq.charAt(prePeptideTrypticIndex);
                }else{
                    prePeptideTrypticChar = '-';
                }
                
                if(postPeptideTrypticIndex < protein_seq.length()){
                    postPeptideTrypticChar = protein_seq.charAt(postPeptideTrypticIndex);
                }else{
                    postPeptideTrypticChar = '-';
                }
                formatted_seq = String.valueOf(prePeptideTrypticChar) + "." +
                        peptide + "." + String.valueOf(postPeptideTrypticChar);
                break;
            }
        }
        
        
        if(formatted_seq.equalsIgnoreCase("un-assigned")){ // implies that peptide could not be found by accession
            // search by protein sequence
            Iterator<Protein> itr2 = proteins.iterator();
            while(itr2.hasNext()){
                Protein protein = itr2.next();
                if(protein.getSequence().contains(peptide)){
                    String protein_seq = protein.getSequence();
                    int peptideStartIndex = protein_seq.indexOf(peptide);
                    int prePeptideTrypticIndex = peptideStartIndex - 1;
                    int postPeptideTrypticIndex = peptideStartIndex + peptide.length();
                    char prePeptideTrypticChar;
                    char postPeptideTrypticChar;
                    if(prePeptideTrypticIndex >= 0){
                        prePeptideTrypticChar = protein_seq.charAt(prePeptideTrypticIndex);
                    }else{
                        prePeptideTrypticChar = '-';
                    }
                
                    if(postPeptideTrypticIndex < protein_seq.length()){
                        postPeptideTrypticChar = protein_seq.charAt(postPeptideTrypticIndex);
                    }else{
                        postPeptideTrypticChar = '-';
                    }
                    formatted_seq = String.valueOf(prePeptideTrypticChar) + "." +
                            peptide + "." + String.valueOf(postPeptideTrypticChar);
                    break;
                }
            }           
        }
        
        if(formatted_seq.equalsIgnoreCase("un-assigned")){ // still couldn't find peptides by protein sequence search
            formatted_seq =  "-." + peptide + ".-" ;
        } 
                
        return formatted_seq;
    }
    
    private String cleanSeq(String rawseq){
        return rawseq.replaceAll(crap,"");
    }
    
}
