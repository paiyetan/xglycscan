/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import db.Database;
import db.fastadb.FastaDb;
import db.searches.psmPeptide.PSMPeptide;
import db.searches.psmPeptide.quan.itraq4plex.PSMPeptideDataGlycSummarizer;
import db.searches.psmPeptide.quan.spectracount.PSMPeptideQuanCountFileObjectExtractor;
import ios.readers.DataPhenoFileReader;
import ios.readers.FastaDbReader;
import ios.readers.MzIdentMLFileReader;
import ios.readers.PSMPeptideFileReader;
import ios.writers.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import xglycscan.enums.*;
import xglycscan.groupindecesacc.GlycCountMatrix;
import xglycscan.groupindecesacc.GlycMatrixExtractor;
import xglycscan.groupindecesacc.GroupIndeces;
import xglycscan.groupindecesacc.GroupIndecesExtractor;
import xglycscan.quan.QuanCountDataMatrix;
import xglycscan.quan.QuanCountDataMatrixExtractor;
import xglycscan.quan.QuanCountFileObject;
import xglycscan.quan.QuanCountGroupObject;
import xglycscan.quan.analysis.*;
import xglycscan.scanner.XGlycScanner;

/**
 *
 * @author paiyeta1
 */
public class XGlycScan {

    
   
    
    // User defined (input/selected) variables
    private InputFileFormatType inFileType;
    private QuantificationType quanType;
    private String outputDir;
    private String phenoFilePath;
    private String searchDBPath;
    private SearchDatabaseType dbtype;
    //private String dataFilePath;
    
    private EvaluationValueType evalueType; //evaluation value not the same as expectation value....
    private boolean computeFDR;
    private double fDRFilter;
    
    private boolean useTopRanked;
    private boolean outputGroupInfo;
    
    //private String currentDirectory = System.getProperty("home.dir");
    private String crap = "[^A-Z]";
    boolean done = false;

    @SuppressWarnings("CallToThreadDumpStack")
    public XGlycScan(InputFileFormatType inFileType, 
                        QuantificationType quanType, 
                        String outputDir, 
                        String phenoFilePath, 
                        String searchDBPath, 
                        SearchDatabaseType dbtype, 
                        //String dataFilePath,
                        EvaluationValueType eValType, 
                        boolean computeFDR, 
                        double fDRFilter,
                        boolean useTopRanked,
                        boolean outputGroupInfo) {
        
            
            this.inFileType = inFileType;
            this.quanType = quanType;
            this.outputDir = outputDir;
            this.phenoFilePath = phenoFilePath;
            this.searchDBPath = searchDBPath;
            this.dbtype = dbtype;
            //this.dataFilePath = dataFilePath;
            
            this.evalueType = eValType;
            this.computeFDR = computeFDR;
            this.fDRFilter = fDRFilter;
            this.useTopRanked = useTopRanked;
            this.outputGroupInfo = outputGroupInfo;
         
    }

    private void scan(String[] inputFiles) throws Exception {
        // Load DB objects
        Database genericDB = new Database(searchDBPath);
        FastaDb fastaDB = FastaDbReader.load(genericDB,dbtype);
        scanPSMPeptidesFiles(inputFiles, 
                                //dataFilePath, 
                                    genericDB, 
                                        fastaDB, dbtype, outputDir, phenoFilePath,
                                            quanType, evalueType, computeFDR, fDRFilter, useTopRanked,outputGroupInfo);
    }


    private String cleanSeq( String rawseq ){
        return rawseq.replaceAll( crap, "" );
    } // end crap


    private ArrayList<PSMPeptide> setGlycosites(ArrayList<PSMPeptide> pDPeps, 
                                                        HashMap dataMap, 
                                                        ArrayList<XGlycScanner.Peptide> peptidesNotFound,
                                                        Database genericDB) {
        // throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<PSMPeptide> mappedPDPeps = new ArrayList<PSMPeptide>();
        XGSPeptideSequenceFormatter formatter = new XGSPeptideSequenceFormatter();
        Iterator<PSMPeptide> itr  = pDPeps.iterator();

        while(itr.hasNext()){
            PSMPeptide pep = itr.next();
            String cleanSequence = cleanSeq(formatter.formatPeptide(pep.getSequence().toUpperCase(),genericDB));

            if (inPeptidesNotFound(cleanSequence, peptidesNotFound)==false){
                // if not in peptides not found, search for mapped glycosite in dataMap
                XGSGlycosite glyc = getGlycosite(cleanSequence,dataMap);
                pep.setGlycosite(glyc);
                mappedPDPeps.add(pep);
            } else {
                XGSGlycosite glyc = new XGSGlycosite("Not_Found",0.00,"IPIXXXXXXXX",0,0,0.00);
                pep.setGlycosite(glyc);
                mappedPDPeps.add(pep);
            } // end if else
        } // end while

        return mappedPDPeps;
    } // end setGlycosites


    private boolean inPeptidesNotFound(String cleanSequence, ArrayList<XGlycScanner.Peptide> peptidesNotFound) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in  = false;
        Iterator<XGlycScanner.Peptide> itr = peptidesNotFound.iterator();
        while(itr.hasNext()){
            XGlycScanner.Peptide pep = itr.next();
            if(pep.getCleanSequence().equalsIgnoreCase(cleanSequence)){
                in = true;
                break;
            } // end if
        } // end while

        return in;

    } // end inPeptidesNotFound


    private XGSGlycosite getGlycosite(String cleanSequence, HashMap dataMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        XGSGlycosite glyc  = new XGSGlycosite("Not_Found",0.00,"IPIXXXXXXXX",0,0,0.00);
        Set entriesMap = dataMap.entrySet();
        Iterator<Map.Entry> itr = entriesMap.iterator();

        while(itr.hasNext()){
            Map.Entry entry = itr.next();
            XGlycScanner.Peptide pep = (XGlycScanner.Peptide) entry.getValue();
            String mapKey = (String) entry.getKey();

            String proteinAccn = mapKey.split("\\.")[0];
            int loc = Integer.parseInt(mapKey.split("\\.")[1]);
            String sequence = pep.getFormattedSequence();
            double probability = pep.getEvalValue();
            int numTrypticEnds = pep.getNumTrypticEnds();
            double mass = pep.getMass();

            if(pep.getCleanSequence().equalsIgnoreCase(cleanSequence)){
                //get all required pep's attribute and place in a new glycosite object
                glyc.setSequence(sequence);
                glyc.setProbability(probability); 
                glyc.setProteinAccn(proteinAccn); 
                glyc.setLocation(loc); 
                glyc.setNumTrypticEnds(numTrypticEnds); 
                glyc.setMass(mass);
                return glyc;
            } else {
                ArrayList<XGlycScanner.Peptide> tossedPeptides = pep.getTossed();
                Iterator<XGlycScanner.Peptide> itr2 = tossedPeptides.iterator();
                while(itr2.hasNext()){
                    XGlycScanner.Peptide ptossed = itr2.next();
                    if(ptossed.getCleanSequence().equalsIgnoreCase(cleanSequence)){
                        //get all required pep's attribute and place in a new glycosite object
                        glyc.setSequence(sequence);
                        glyc.setProbability(probability); 
                        glyc.setProteinAccn(proteinAccn); 
                        glyc.setLocation(loc); 
                        glyc.setNumTrypticEnds(numTrypticEnds); 
                        glyc.setMass(mass);
                        return glyc;
                    }
                }
            }    
        }
        return glyc;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void scanPSMPeptidesFiles(String[] files2Scan, 
                                        //String dataFile, 
                                        Database genericDB, 
                                        FastaDb fastaDB,
                                        SearchDatabaseType dbtype, 
                                        String outputDir,
                                        String phenoFilePath,
                                        QuantificationType quanType,
                                        EvaluationValueType evalueType, 
                                        boolean computeFDR, 
                                        double fDRFilter,
                                        boolean useTopRanked,
                                        boolean outputGroupInfo) throws FileNotFoundException {

        // global collection object(s)... An arrayList of arrayList of all uniquely identified glycosites in each input file (file2Scan)
        ArrayList<ArrayList<PSMPeptide>> summarizedPepPeps = null; // for iTRAQ4plex quantification
        ArrayList<QuanCountFileObject> quanCountFileObjs = null; //for spectra_count quantification
        // map of file name to QuanCountFile objects...
        HashMap<String,QuanCountFileObject> file2QuanCountObjMap = new HashMap<String,QuanCountFileObject>();
        // map of file to sample (phenotype mapping...)
        HashMap<String,String> file2SampleMap = null;
        DataPhenoFileReader pDataReader;

        ArrayList<XGSFileIDIndeces> iDIndeces = new ArrayList<XGSFileIDIndeces>(); 
        // Determine quantification type and initiate the respective global collection object... 
        switch(quanType){
            case iTRAQ4plex: // for downstream version implementation...
                summarizedPepPeps = new ArrayList<ArrayList<PSMPeptide>>(); 
                pDataReader = new DataPhenoFileReader();
                if(phenoFilePath==null){
                    file2SampleMap = pDataReader.autoGeneratePhenoMap(files2Scan, quanType);
                } else {
                    if(pDataReader.fileIsWellFormed(phenoFilePath, quanType)){
                    file2SampleMap = pDataReader.readiTRAQ4plexPhenoFile(phenoFilePath);
                    } else {
                        file2SampleMap = pDataReader.autoGeneratePhenoMap(files2Scan, quanType);
                    }// end if_else
                }
                break;

            default: //SPECTRA_COUNT
                quanCountFileObjs = new ArrayList<QuanCountFileObject>();
                pDataReader = new DataPhenoFileReader();
                if(phenoFilePath==null){
                    file2SampleMap = pDataReader.autoGeneratePhenoMap(files2Scan, quanType);
                } else {
                    if(pDataReader.fileIsWellFormed(phenoFilePath, quanType)){
                        file2SampleMap = pDataReader.readSpectraCountPhenoFile(phenoFilePath);
                    } else {
                        System.out.println("  Phenotype input isn't well-formed, thus auto-generating file to phenotype map...");
                        file2SampleMap = pDataReader.autoGeneratePhenoMap(files2Scan, quanType);
                        System.out.println("  Auto-generated phenotype map created...");                      
                    }// end if_else
                }// end if_else
                break;
        }   

        String outTableDir = outputDir + File.separator + "xGlycscan.tables";
        if(new File(outTableDir).exists()==false){
            new File(outTableDir).mkdirs();
        }
        
        int preFileIndex = 10; // for GUI implementation...
        int incrementFactor = 75/files2Scan.length; //for GUI implementation...
        
        // scan individual files for XGSFilePeptidePSM objects
        for(int i = 0; i < files2Scan.length; i++){
            System.out.println((i+1) + ": " + new File(files2Scan[i]).getName());
            preFileIndex = preFileIndex + incrementFactor;            
            try {
                String fileName = files2Scan[i];
                
                // Read file
                System.out.println("  Reading file...");
                ArrayList<PSMPeptide> psmPeptides = null;
                switch(inFileType){ 
                    //M2LITE_THERMOPD_PSM, MZIDENTML, PEPXML, MSF 
                    case M2LITE_THERMOPD_PSM:
                        System.out.println("  Extracting \"PSMPeptide\" objects...");
                        PSMPeptideFileReader pDFileReader = new PSMPeptideFileReader();
                        psmPeptides = pDFileReader.readAnyThermoPSMTextFile(fileName);
                        
                        break;
                        
                    default: // MZIDENTML:
                        System.out.println("  Extracting \"PSMPeptide\" objects...");
                        MzIdentMLFileReader mzidFileReader = new MzIdentMLFileReader();
                        psmPeptides = mzidFileReader.read(fileName, 
                                                            evalueType, 
                                                                computeFDR, 
                                                                    fDRFilter,
                                                                        outTableDir,
                                                                            useTopRanked);
                        break;
                }
                
                // Generate idetified glycosites and "not found" Peptide lists
                XGlycScanner scanner = new XGlycScanner(fileName, psmPeptides, 
                                                            //dataFile, 
                                                            genericDB, fastaDB, dbtype, outputDir); 
                HashMap dataMap = scanner.getDataMap();
                ArrayList<XGlycScanner.Peptide> peptidesNotFound = scanner.getNotFound();

                // Set result peptide XGSGlycosite
                System.out.println("  Mapping identified glycosites back to PSMPeptide objects...");
                ArrayList<PSMPeptide> mappedPSMPeps = setGlycosites(psmPeptides, dataMap, peptidesNotFound, genericDB);

                // reprint PSMPeptide File with mapped XGSGlycosite.
                // Set result peptide XGSGlycosite
                System.out.println("  Re-Printing PSMPeptide objects with mapped identified glycosites...");
                PSMPeptideMappedPeptidePrinter printer = new PSMPeptideMappedPeptidePrinter();

                //make an output table name
                //make a Glycs directory in outputTables directory
                String glycsTableDir = outTableDir + File.separator + "glycs";
                if(new File(glycsTableDir).exists()==false){
                    new File(glycsTableDir).mkdirs();
                }
                String outTable = glycsTableDir + File.separator + 
                        new File(fileName).getName().replace(".msfparser.psms",".glycs").replace(".mzid", ".glycs");
                printer.print(outTable,mappedPSMPeps,quanType);


                System.out.println("  Extracting file-specific identification indeces...");
                // this includes 1. numberOfIDPeptides, 2. numberOfUniqIDPeptides, 3. numberOfIDGlycosites;
                // 4. numberOfUniqIDGlycosites and 5. Specificity;
                XGSFileIDIndeces indeces = new XGSFileIDIndeces();
                indeces.extractXGSFileIDIndeces(fileName, mappedPSMPeps);
                iDIndeces.add(indeces);

                // Quantifification:
                // Determine quantification type and initiate the respective global collection object... 
                switch(quanType){

                    case iTRAQ4plex: //for downstream implementation...
                        //preprocess the data:
                            //1. Summarize
                            //2. +/- Normalize (default is to not normalize)
                        PSMPeptideDataGlycSummarizer summarizer = new PSMPeptideDataGlycSummarizer();
                        ArrayList<PSMPeptide> summPeps = summarizer.summarize(mappedPSMPeps);

                        // **** in subsequent implementation, check if user opted to normalize the data or not at this step **** //

                        summarizedPepPeps.add(summPeps);                                            
                        break;

                    default: //case SPECTRA_COUNT:
                        PSMPeptideQuanCountFileObjectExtractor quanObjExt = new PSMPeptideQuanCountFileObjectExtractor();
                        String file = new File(fileName).getName();
                        QuanCountFileObject quanCountObj = quanObjExt.extractQuanObject(file, mappedPSMPeps);
                        quanCountFileObjs.add(quanCountObj);
                        // map file to QuanCountObject
                        file2QuanCountObjMap.put(file, quanCountObj);
                        break;
                }                

            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println();
        // Print file(s) identification indeces
        System.out.println("Printing collective file(s) indeces...");
        FileIDIndecesPrinter printer = new FileIDIndecesPrinter();
        printer.print(iDIndeces, outTableDir + File.separator + "identification.indeces");
        System.out.println();
        // Extract Quantification Matrix
        System.out.println("Estimating quantification values...");
        QuanCountDataMatrixExtractor mExt = new QuanCountDataMatrixExtractor(quanCountFileObjs, file2SampleMap);
        System.out.println("Retrieving quantification matrix...");
        QuanCountDataMatrix quanMat = mExt.getQuanCountDataMatrix();
        System.out.println("Printing quantification matrix...");
        DataMatrixPrinter dMPrinter = new DataMatrixPrinter();
        dMPrinter.print(quanMat, file2SampleMap, false, outTableDir + File.separator + "spectraCount.matrix");
        System.out.println();
        
        //System.out.println("Retrieving group quantification matrix...");
        //QuanCountDataMatrix grpQuanMat = mExt.getGroupsQuanCountDataMatrix();

        // *********** Independent File-Level Analyses **************** //
        AnalysisResultPrinter analysisPrinter = new AnalysisResultPrinter();
        System.out.println();
        // --- Perform individual File-Based Analysis ---
        QuanCountFilesAnalyzer qCAnalyzer = new QuanCountFilesAnalyzer(quanCountFileObjs, file2QuanCountObjMap);
        // Estimate file coefficients 
        System.out.println("Estimating file(s) identification coefficients...");
        HashMap<String, QuanCountUniqGlycNCoefObject> file2GlycNCoefMap = qCAnalyzer.getFile2UniqGlyNCoefMap();
        // Print calculated file coefficients
        System.out.println("Printing file(s) identification coefficients...");
        analysisPrinter.printIdentificationCoef(file2GlycNCoefMap, outTableDir + File.separator + "identification.coef");
        // Estimate file similarities
        System.out.println("Estimating file(s) identification similarities...");
        QuanSquareMatrix countSimMatrix = qCAnalyzer.getCountsSimilarityMatrix();
        System.out.println("Printing file(s) identification similarities...");
        analysisPrinter.printSquareMatrix(countSimMatrix, MatrixType.INTEGER, outTableDir + File.separator + "iDOverlapCount.matrix");
        // Estimate file similarities
        System.out.println("Estimating file(s) identification percent similarities...");
        QuanSquareMatrix percentSimMatrix = qCAnalyzer.getPercentSimilarityMatrix();
        System.out.println("Printing file(s) identification percent similarities...");
        analysisPrinter.printSquareMatrix(percentSimMatrix, MatrixType.DOUBLE, outTableDir + File.separator + "iDOverlapPercent.matrix");
        // ********************************************************************************* //
         
        // **************** Group Indeces Summary ************************ //
        if(outputGroupInfo){
            //System.out.println("Printing group quantification matrix...");
            String groupOutTableDir = outTableDir + File.separator + "groups";
            if(new File(groupOutTableDir).exists()==false){
                new File(groupOutTableDir).mkdirs();
            }
            //extract group indeces...
            System.out.println("  Extracting group identification indeces...");
            HashMap<String, LinkedList<String>> pheno2FilesMap = mapPheno2Files(file2SampleMap);
            GroupIndecesExtractor extractor = new GroupIndecesExtractor();
            ArrayList<GroupIndeces> groupIndeces = 
                    extractor.extractGroupIndeces(pheno2FilesMap, iDIndeces, groupOutTableDir);//glycDir.getOutputDir(),outDir.getOutputDir());
            //print group indeces...        
            System.out.println("  Printing group identification indeces...");
            GroupGlycIndecesPrinter indecesPrinter = new GroupGlycIndecesPrinter();
            indecesPrinter.print(groupIndeces, groupOutTableDir + File.separator + "group.identification.indeces");
            //extract group spectra_count matrix object
            System.out.println("  Extracting group spectra_count matrix...");
            GlycMatrixExtractor matrixExtractor = new GlycMatrixExtractor(groupIndeces);
            GlycCountMatrix countMatrix = matrixExtractor.getGlycCountMatrix();
            //print spectra_count matrix
            System.out.println("  Printing group spectra_count matrix...");
            GlycMatrixPrinter matrixPrinter = new GlycMatrixPrinter();
            matrixPrinter.print(countMatrix, groupOutTableDir + File.separator + "group.spectraCount.matrix");
            
            // ----- for later implementation(s) ----- //
            //     Estimate file differences              
            // --------------------------------------- //
            boolean doGroupAnalysis = false; //defaults to false in this implementation.
            System.out.println();
            // --- Perform Group-Based Analysis --- 
            // instantiate group analyzer object  
            if(doGroupAnalysis){
                QuanCountGroupsAnalyzer qCGAnalyzer = new QuanCountGroupsAnalyzer(file2SampleMap, file2QuanCountObjMap);
                if(qCGAnalyzer.getPhenotypes().length != file2QuanCountObjMap.size()){ //only perform group analysis if files are grouped
                    System.out.println();
                    // --- Between groups analysis
                    // Estimate group similarities count 
                    System.out.println("Estimating between group(s) identification similarities...");
                    QuanSquareMatrix grpCountSimMatrix = qCGAnalyzer.getCountsSimilarityMatrix();
                    System.out.println("Printing between group(s) identification similarities...");
                    analysisPrinter.printSquareMatrix(grpCountSimMatrix, MatrixType.INTEGER, groupOutTableDir + File.separator + "groupIDOverlapCount.matrix");

                    // Estimate group similarities percent 
                    System.out.println("Estimating between group(s) identification percent similarities...");
                    QuanSquareMatrix grpPercentSimMatrix = qCGAnalyzer.getPercentSimilarityMatrix();
                    System.out.println("Printing between group(s) identification percent similarities...");
                    analysisPrinter.printSquareMatrix(grpPercentSimMatrix, MatrixType.DOUBLE, groupOutTableDir + File.separator + "groupIDOverlapPercent.matrix");

                    // Estimate group identification difference
                    System.out.println("Estimating between group(s) identification difference...");
                    QuanSquareMatrix grpCountDiffMatrix = qCGAnalyzer.getCountsDiffMatrix();
                    System.out.println("Printing between group(s) identification difference...");
                    analysisPrinter.printSquareMatrix(grpCountDiffMatrix, MatrixType.INTEGER, groupOutTableDir + File.separator + "groupIDDiffCount.matrix");

                    // ----- for later implementation(s) ----- //
                    // Estimate other group differences             
                    // --------------------------------------- //
                    // --- Within group analysis ---
                    System.out.println();
                    System.out.println("Computing within groups' analyses...");
                    HashMap<String,ArrayList<QuanCountFileObject>> pheno2QuanCountFileObjsMap = qCGAnalyzer.getPheno2QuanCountFileObjsMap();
                    Set<String> groups = pheno2QuanCountFileObjsMap.keySet();
                    Iterator<String> itr = groups.iterator();
                    int groupIndex = 1;
                    while(itr.hasNext()){
                        String group = itr.next();
                        System.out.println(groupIndex + ": " + group);
                        // get within group file(s)
                        ArrayList<QuanCountFileObject> qcFObjs = pheno2QuanCountFileObjsMap.get(group);
                        // perform within group analysis on files of group, thus instantiate "file analyzer" on within group fileQuan objects 
                        QuanCountFilesAnalyzer withinGrpQCFilesAnalyzer = new QuanCountFilesAnalyzer(group, qcFObjs, file2QuanCountObjMap);
                        // set within group table
                        String withinGroupOutTableDir = groupOutTableDir + File.separator + group;
                        if(new File(withinGroupOutTableDir).exists()==false){
                            new File(withinGroupOutTableDir).mkdirs();
                        }
                        // Estimate file coefficients 
                        System.out.println(" Estimating " + group + "'s file(s) identification coefficients...");
                        HashMap<String, QuanCountUniqGlycNCoefObject> groupfile2GlycNCoefMap = withinGrpQCFilesAnalyzer.getFile2UniqGlyNCoefMap();
                        // Print calculated file coefficients
                        System.out.println(" Printing " + group + "'s file(s) identification coefficients...");
                        analysisPrinter.printIdentificationCoef(groupfile2GlycNCoefMap, withinGroupOutTableDir + File.separator + "identification.coef");
                        // Estimate file similarities
                        System.out.println(" Estimating " + group + "'s file(s) identification similarities...");
                        QuanSquareMatrix groupCountSimMatrix = withinGrpQCFilesAnalyzer.getCountsSimilarityMatrix();
                        System.out.println(" Printing " + group + "'s file(s) identification similarities...");
                        analysisPrinter.printSquareMatrix(groupCountSimMatrix, MatrixType.INTEGER, withinGroupOutTableDir + File.separator + "iDOverlapCount.matrix");

                        // Estimate file similarities
                        System.out.println(" Estimating " + group + "'s file(s) identification percent similarities...");
                        QuanSquareMatrix groupPercentSimMatrix = withinGrpQCFilesAnalyzer.getPercentSimilarityMatrix();
                        System.out.println(" Printing " + group + "'s file(s) identification percent similarities...");
                        analysisPrinter.printSquareMatrix(groupPercentSimMatrix, MatrixType.DOUBLE, 
                                                withinGroupOutTableDir + File.separator + "iDOverlapPercent.matrix");

                        // Estimate groups cummalative/differential run effect
                        System.out.println(" Estimating " + group + "'s file(s) cumultive/differential run effect...");
                        QuanCountGroupObject qcGObj = new QuanCountGroupObject(group, qcFObjs);
                        ArrayList<QuanCountFileCumRunEffectObject> qcFCREObjs = qcGObj.getCummEffectObjects();
                        System.out.println(" Printing " + group + "'s file(s) cumultive/differential run effect...");
                        analysisPrinter.printGroupCummulativeFileEffects(qcFCREObjs,
                                                withinGroupOutTableDir + File.separator + "CummGroupFileEffects.diff");

                        // ----- for later implementation(s) ----- //
                        // Estimate file differences               //
                        // --------------------------------------- //

                        groupIndex++;
                    }
                }
            }          
        }        
    }

    public String getCrap() {
        return crap;
    }
    
    // ******** Main method Helper (Static) methods **********
    private static HashMap<String, String> getConfig() throws FileNotFoundException, IOException {
        HashMap<String,String> configMap = new HashMap<String,String>();
        BufferedReader reader = new BufferedReader(new FileReader("./XGlycScan2.0.config"));
        String line;
        while((line = reader.readLine())!=null){
            String[] lineArr = line.split("=");
            configMap.put(lineArr[0], lineArr[1]);
        }       
        return configMap;
    }

    private static InputFileFormatType getInputFileType(HashMap<String, String> configMap) {
        
        InputFileFormatType inFileType = null;
        String inFileFormat = configMap.get("InputFileType");
        
        if(inFileFormat.equalsIgnoreCase("MZIDENTML")){ // implementation's default
            inFileType = InputFileFormatType.MZIDENTML;            
        } 
                
        return inFileType;
                
    }

    private static QuantificationType getQuantificationType(HashMap<String, String> configMap) {
        QuantificationType quanType = null;
        String qType = configMap.get("QuantificationType");
        
        if(qType.equalsIgnoreCase("SPECTRA_COUNT")){ // implementation's default
            quanType = QuantificationType.SPECTRA_COUNT;
        }
        return quanType;
    }

    private static SearchDatabaseType getDatabaseType(HashMap<String, String> configMap) {
        SearchDatabaseType dbType = null;//throw new UnsupportedOperationException("Not yet implemented");
        String dbTypeString = configMap.get("SearchDBType");
        
        if(dbTypeString.equalsIgnoreCase("REFSEQ")){
            dbType = SearchDatabaseType.REFSEQ;
        } else if(dbTypeString.equalsIgnoreCase("IPIv387")){
            dbType = SearchDatabaseType.IPIv387;
        }
        return dbType;
    }
    
    private static QuantificationType getQuanType(InputFileFormatType inFileType, HashMap<String, String> configMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        QuantificationType qType = null;
        if(inFileType == InputFileFormatType.MZIDENTML){
            qType = QuantificationType.SPECTRA_COUNT;
        } else {
            qType = getQuantificationType(configMap);
        }
        
        return qType;
    }
    
    private static EvaluationValueType getEvalValueType(HashMap<String, String> configMap) {
        EvaluationValueType eVType = null;
        String eValueType = configMap.get("eValueType");
        if(eValueType.equalsIgnoreCase("SEQUESTXCORR")){
            eVType = EvaluationValueType.SEQUESTXCORR;
        }
        return eVType;
    }

    private static boolean computeFDR(HashMap<String, String> configMap) {
        boolean fDR = false;
        String computeFDR = configMap.get("computeFDR");
        if(computeFDR.equalsIgnoreCase("TRUE")){
            fDR = true;
        }
        return fDR;
    }
    
    private static boolean useTopRanked(HashMap<String, String> configMap){
        boolean useTopR = false;
        String useTopRanked = configMap.get("useTopRanked");
        if(useTopRanked.equalsIgnoreCase("TRUE")){
            useTopR = true;
        }
        return useTopR;
    }

    

    
    
    public static void main(String[] args) throws FileNotFoundException, IOException, Exception{
        // args-obtained local attributes
        System.out.println("Starting...");
        String[] inFiles = null;
        String outputDir = null;
        String phenoDataFilePath = null;
        
        // config-file-obtained local attrbutes
        //  Paths
        String searchDBPath;
        //String dataFilePath;
        //  EnumTypes
        InputFileFormatType inFileType;
        QuantificationType quanType;
        SearchDatabaseType dbType;
        EvaluationValueType eValType;
          
        boolean computeFDR;
        double fDRFilter;        
        boolean useTopRanked;  
        
        boolean outputGroupInfo = false;
        
        // retrieve arguments
        String inFileOrDirArg = args[0]; 
        outputDir = args[1];
        if(args.length > 2){
            phenoDataFilePath = args[2];
            outputGroupInfo = true;
        }
        // get inputFile(s)
        System.out.println(" Files to scan...");
        File inFileOrDir = new File(inFileOrDirArg);
        if(inFileOrDir.isFile()){
            inFiles = new String[1];
            inFiles[0] = inFileOrDir.getAbsolutePath();
            System.out.println("  " + inFileOrDir.getName());
        } else if(inFileOrDir.isDirectory()){
            //get .mzid files in the directory
            File[] filesInDir = inFileOrDir.listFiles();
            ArrayList<String> filesInD = new ArrayList<String>();
            for(int i = 0; i < filesInDir.length; i++){
                if(filesInDir[i].getAbsolutePath().endsWith("mzid")){
                    filesInD.add(filesInDir[i].getAbsolutePath());
                }
            }
            inFiles = new String[filesInD.size()];
            for(int i = 0; i < inFiles.length; i++){
                inFiles[i] = filesInD.get(i);
                System.out.println("  " + (i + 1) + ": " + new File(inFiles[i]).getName());
            }
        } else {
            System.out.println("ERROR: First argument must be an \"mzIdentML (.mzid)\" file or an \"mzIdentML-containing\" directory ...");
            System.exit(1);
        }
        
        //read (retrieve) program configuration(s)
        HashMap<String,String> configMap = getConfig(); 
        searchDBPath = configMap.get("SearchDBPath");
        //dataFilePath = configMap.get("DataFilePath");
        
        //get/set InputFileType, QuantificationType, SearchDBType, EvaluationValueType
        inFileType = getInputFileType(configMap);
        quanType = getQuanType(inFileType,configMap);
        dbType = getDatabaseType(configMap);
        eValType = getEvalValueType(configMap);
        
        computeFDR = computeFDR(configMap);
        fDRFilter = Double.parseDouble(configMap.get("fDRFilter"));
        
        useTopRanked = useTopRanked(configMap);
                
        
        
        System.out.println("Scanning...");
        Date start_time = new Date();
        long start = start_time.getTime();
        
        // instantiate xGScan and initiate global variables...
        XGlycScan xgscan = new XGlycScan(inFileType, quanType, outputDir, phenoDataFilePath, 
                                            searchDBPath, dbType, 
                                                //dataFilePath,
                                                //values for use by MZIdenMLFileReader
                                                eValType, computeFDR, fDRFilter, useTopRanked, outputGroupInfo);
        // scan files...
        xgscan.scan(inFiles);      
        // finish...
        System.out.println("\n...Done!!!");
        Date end_time = new Date();
        long end = end_time.getTime();
        System.out.println("End: " + end + ": " + end_time.toString());
        System.out.println("Total time: " + (end - start) + " milliseconds; " + 
                        TimeUnit.MILLISECONDS.toMinutes(end - start) + " min(s), "
                        + (TimeUnit.MILLISECONDS.toSeconds(end - start) - 
                           TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start))) + " seconds.");
     }

    private HashMap<String, LinkedList<String>> mapPheno2Files(HashMap<String, String> file2SampleMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        System.out.println(" Mapping files to phenotype...");
        HashMap<String, LinkedList<String>> pheno2Files = new HashMap<String, LinkedList<String>>();
        Set<String> files = file2SampleMap.keySet();
        for(String file: files){
            String pheno = file2SampleMap.get(file);
            if(pheno2Files.containsKey(pheno)){
                LinkedList<String> mappedFiles = pheno2Files.remove(pheno);
                if(mappedFiles.contains(file) == false){
                    mappedFiles.add(file);
                    System.out.println("   " + file + " is mapped to " + pheno);
                }
                pheno2Files.put(pheno, mappedFiles);
            }else{
                LinkedList<String> mappedFiles = new LinkedList<String>();
                mappedFiles.add(file);
                System.out.println("   " + file + " is mapped to " + pheno);
                pheno2Files.put(pheno, mappedFiles);
            }
        }
        return pheno2Files;
    }
    
    
    
}