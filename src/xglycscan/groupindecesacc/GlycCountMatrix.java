/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.groupindecesacc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author paiyeta1
 */
public class GlycCountMatrix {
    
    private String[] glycosites;
    private String[] identifiers; //group (pheno) or file identifiers
    private int[][] matrix;

    public GlycCountMatrix(HashMap<String,GroupIndeces> iD2IndecesMap,
                                    HashMap<String,LinkedList<String>> glyc2IDMap) { 
        setGlycArray(glyc2IDMap);
        setIdeitifiers(iD2IndecesMap);
        initializeMatrix();
        populateMatrix(glyc2IDMap,iD2IndecesMap);
    }

    public GlycCountMatrix(String[] phenotypes, String[] glycositeSeqs, int[][] matrix) {
        //throw new UnsupportedOperationException("Not yet implemented");
        identifiers = phenotypes;
        glycosites = glycositeSeqs;
        this.matrix = matrix;
    }

    private void setGlycArray(HashMap<String, LinkedList<String>> glyc2IDMap) {
        
        Set<String> glycs = glyc2IDMap.keySet();
        glycosites = new String[glycs.size()];
        Iterator<String> itr = glycs.iterator();
        int index = 0;
        while(itr.hasNext()){
            String glyc = itr.next();
            glycosites[index] = glyc;
            index++;           
        }
    }

    private void setIdeitifiers(HashMap<String,GroupIndeces> iD2IndecesMap) {
        
        Set<String> mapKeys = iD2IndecesMap.keySet();
        identifiers = new String[mapKeys.size()];
        Iterator<String> itr = mapKeys.iterator();
        int index = 0;
        while(itr.hasNext()){
            String key = itr.next();
            identifiers[index] = key;
            index++;           
        }
    }
    
    private void initializeMatrix() {
        matrix = new int[glycosites.length][identifiers.length];
        //initialize array
        for(int i = 0; i < glycosites.length; i++){
            for(int j = 0; j <identifiers.length; j ++){
                matrix[i][j] = 0;
            }
        }   
    }

    private void populateMatrix(HashMap<String, LinkedList<String>> glyc2IDMap,
                                    HashMap<String,GroupIndeces> iD2IndecesMap) {
        
        for(int i = 0; i < glycosites.length; i++){
            String glyc = glycosites[i];
            for(int j = 0; j < identifiers.length; j++){
                String identifier = identifiers[j]; // file or group identifier
                int glycosite_count = 0;
                LinkedList<String> mappedGroupIndecesIDs = glyc2IDMap.get(glyc);
                Iterator<String> itr = mappedGroupIndecesIDs.iterator();
                while(itr.hasNext()){
                    String mappedGroupIndecesID = itr.next();
                    if(mappedGroupIndecesID.equalsIgnoreCase(identifier)){
                        GroupIndeces groupInd = iD2IndecesMap.get(mappedGroupIndecesID);
                        if(groupInd.getGlyc2Count().containsKey(glyc)){
                            glycosite_count = groupInd.getGlyc2Count().get(glyc);
                            break;
                        }
                    }
                }
                //double channel_value
                matrix[i][j] = glycosite_count;
            }
        }
    }

    public String[] getIdentifiers() {
        return identifiers;
    }

    public String[] getGlycosites() {
        return glycosites;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    
    
    
    
}
