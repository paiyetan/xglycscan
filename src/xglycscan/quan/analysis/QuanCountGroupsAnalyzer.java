/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan.analysis;

import java.util.*;
import xglycscan.quan.QuanCountFileObject;
import xglycscan.quan.QuanCountGroupObject;

/**
 *
 * @author paiyeta1
 */
public class QuanCountGroupsAnalyzer {
    
    private String[] phenotypes;
    private HashMap<String,LinkedList<String>> pheno2FilesMap;
    private HashMap<String,ArrayList<QuanCountFileObject>> pheno2QuanCountFileObjsMap;
    private HashMap<String,QuanCountGroupObject> pheno2QuanCountGroupObjMap;
    
    

    public QuanCountGroupsAnalyzer (HashMap<String, String> file2SampleMap, 
            HashMap<String,QuanCountFileObject> file2QuanCountObjMap) {        
        extractPhenotypes(file2SampleMap);
        mapPheno2Files(file2SampleMap);
        mapPheno2QuanCountFileObjs(file2QuanCountObjMap);
        mapPheno2QuanCountGroupObj();
        
    }

    
    private void extractPhenotypes(HashMap<String, String> file2SampleMap) {
        Set<String> filesMapped = file2SampleMap.keySet();
        ArrayList<String> phenotypeArrList = new ArrayList<String>();
        Iterator<String> itr = filesMapped.iterator();
        while(itr.hasNext()){
            String mappedFile =  itr.next();
            String phenotype = file2SampleMap.get(mappedFile);
            if(phenotypeArrList.contains(phenotype) == false){
                phenotypeArrList.add(phenotype);
            }
        }
        phenotypes = toArray(phenotypeArrList); 
        System.out.println("   " + phenotypes.length + " phenotype(s)/group(s) found");
    }
    private String[] toArray(ArrayList<String> phenotypeArrList) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String[] arr = new String[phenotypeArrList.size()];
        for(int i = 0; i < arr.length; i++){
            arr[i] = phenotypeArrList.get(i);
            System.out.println("   " + arr[i] + " phenotype/group found...");
        }
        return arr;
    }
    
    private void mapPheno2Files(HashMap<String, String> file2SampleMap) {
        pheno2FilesMap = new HashMap<String,LinkedList<String>>();
        Set<String> files = file2SampleMap.keySet();
        for(int i = 0; i < phenotypes.length; i++){
            String phenotype = phenotypes[i];
            LinkedList<String> mappedFiles = null;
            Iterator<String> itr = files.iterator();
            while(itr.hasNext()){
                String file = itr.next();
                if(file2SampleMap.get(file).equalsIgnoreCase(phenotype)){
                    //check if a sample/phenotype to file(s) mapping is in the map
                    if(pheno2FilesMap.containsKey(phenotype)){
                        mappedFiles = pheno2FilesMap.remove(phenotype);
                        mappedFiles.add(file);
                        pheno2FilesMap.put(phenotype, mappedFiles);
                    } else {
                        mappedFiles = new LinkedList<String>();
                        mappedFiles.add(file);
                        pheno2FilesMap.put(phenotype, mappedFiles);
                    }
                }
            }
            // *** debugging script ***
            System.out.println("   " + mappedFiles.size() + " file(s):");
            for(int j = 0; j < mappedFiles.size(); j++) {
                System.out.println("      " + mappedFiles.get(j));
            }
            System.out.println("      mapped to " + phenotype + " phenotype/group...");
        }
        
    }

    private void mapPheno2QuanCountFileObjs(HashMap<String,QuanCountFileObject> file2QuanCountObjMap) {
        pheno2QuanCountFileObjsMap = new HashMap<String,ArrayList<QuanCountFileObject>>();
        for(int i = 0; i < phenotypes.length; i++){
            String phenotype = phenotypes[i];
            // get phenoMappedFiles
            LinkedList<String> phenoMappedFiles = pheno2FilesMap.get(phenotype);
            ArrayList<QuanCountFileObject> phenoMappedQuanFiles = new ArrayList<QuanCountFileObject>();
            // for each phenoMappedFile, get the corresponding QuanCountFileObject and append to the arrayList object phenoMappedQuanFiles
            Iterator<String> itr = phenoMappedFiles.iterator();
            while(itr.hasNext()){
                String file = itr.next();
                QuanCountFileObject qCFObj = file2QuanCountObjMap.get(file);
                phenoMappedQuanFiles.add(qCFObj);
            }
            pheno2QuanCountFileObjsMap.put(phenotype, phenoMappedQuanFiles);
            // *** debugging script ***
            System.out.println("   " + phenoMappedQuanFiles.size() + " Quan file(s) with fileID(s):");
            for(int j = 0; j < phenoMappedQuanFiles.size(); j++) {
                System.out.println("      " + phenoMappedQuanFiles.get(j).getFilename());
            }
            System.out.println("      mapped to " + phenotype + " phenotype/group...");
        }
        
    }

    private void mapPheno2QuanCountGroupObj() {
        //throw new UnsupportedOperationException("Not yet implemented");
        pheno2QuanCountGroupObjMap = new HashMap<String,QuanCountGroupObject>();
        for(int i = 0; i < phenotypes.length; i++){
            String phenotype = phenotypes[i];
            ArrayList<QuanCountFileObject> qCFObjs = pheno2QuanCountFileObjsMap.get(phenotype);
            System.out.println("   " + qCFObjs.size() + " QuanCountFileObject(s) were found mapped to " + phenotype);
            QuanCountGroupObject qCGObj = new QuanCountGroupObject(phenotype,qCFObjs);
            pheno2QuanCountGroupObjMap.put(phenotype, qCGObj);
        }
    }
    
    public HashMap<String, LinkedList<String>> getPheno2FilesMap() {
        return pheno2FilesMap;
    }

    public HashMap<String, ArrayList<QuanCountFileObject>> getPheno2QuanCountFileObjsMap() {
        return pheno2QuanCountFileObjsMap;
    }

    public HashMap<String, QuanCountGroupObject> getPheno2QuanCountGroupObjMap() {
        return pheno2QuanCountGroupObjMap;
    }

    public String[] getPhenotypes() {
        return phenotypes;
    }
    
    public QuanSquareMatrix getCountsSimilarityMatrix() {
        QuanSquareMatrix qCSimMatrix = null;
        int[][] countsSimMatrix = new int[phenotypes.length][phenotypes.length];
        for(int i = 0; i < phenotypes.length; i++){
            for(int j = 0; j < phenotypes.length; j++){
                QuanCountGroupObject qcGrpObjI = pheno2QuanCountGroupObjMap.get(phenotypes[i]);
                QuanCountGroupObject qcGrpObjJ = pheno2QuanCountGroupObjMap.get(phenotypes[j]);
                countsSimMatrix[i][j] = qcGrpObjI.intersect(qcGrpObjJ);
            }
        }
        qCSimMatrix = new QuanSquareMatrix(phenotypes,countsSimMatrix);
        return qCSimMatrix;
    }
    
    public QuanSquareMatrix getPercentSimilarityMatrix() {
        QuanSquareMatrix qCSimMatrix = null;
        double[][] doubleSimMatrix = new double[phenotypes.length][phenotypes.length];
        for(int i = 0; i < phenotypes.length; i++){
            for(int j = 0; j < phenotypes.length; j++){
                QuanCountGroupObject qcGrpObjI = pheno2QuanCountGroupObjMap.get(phenotypes[i]);
                QuanCountGroupObject qcGrpObjJ = pheno2QuanCountGroupObjMap.get(phenotypes[j]);
                doubleSimMatrix[i][j] = qcGrpObjI.similarity(qcGrpObjJ);
            }
        }
        qCSimMatrix = new QuanSquareMatrix(phenotypes,doubleSimMatrix);
        return qCSimMatrix;
    }
    
    public QuanSquareMatrix getCountsDiffMatrix() {
        QuanSquareMatrix qCSimMatrix = null;
        int[][] countsDiffMatrix = new int[phenotypes.length][phenotypes.length];
        for(int i = 0; i < phenotypes.length; i++){
            for(int j = 0; j < phenotypes.length; j++){
                QuanCountGroupObject qcGrpObjI = pheno2QuanCountGroupObjMap.get(phenotypes[i]);
                QuanCountGroupObject qcGrpObjJ = pheno2QuanCountGroupObjMap.get(phenotypes[j]);
                countsDiffMatrix[i][j] = qcGrpObjI.diff(qcGrpObjJ);
            }
        }
        qCSimMatrix = new QuanSquareMatrix(phenotypes,countsDiffMatrix);
        return qCSimMatrix;
    }
    
    

    
}



