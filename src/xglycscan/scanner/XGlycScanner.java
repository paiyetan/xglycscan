/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.scanner;

import db.Database;
import db.fastadb.FastaDb;
import db.fastadb.FastaDbEntry;
import db.searches.psmPeptide.PSMPeptide;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import xglycscan.XGSFilePeptidePSM;
import xglycscan.XGSFilePeptidePSMExtractor;
import xglycscan.enums.SearchDatabaseType;


/**
 *
 * @author paiyeta1
 */

public class XGlycScanner extends Object{
    
    public final static int TISSUE_NONE = 0;
    public final static int TISSUE_AS_INT_BY_SOURCE = 3;
    public final static String DUMMY_SEQ = "...";
    public final static String TYPE_INTERACT = "Interact File";
    public final static String TYPE_SEQ = "Sequence List";
    public final static String TYPE_IPI = "IPI List";

    private FastaDb db;
    //private String dataFile; //existing datafile    
    private String file;

    // for cleaning the peptide sequence....
    private boolean tissueSpecified = false;
    private String crap = new String( "[^A-Z]" );
    private Pattern nxts = Pattern.compile( "N([A-Z]|M\\#|M\\*)(T|S)" );

    // Maps an ipi.nxts site to it's peptide object
    private HashMap dataMap = new HashMap();
    // ArrayList of Peptides not found...
    ArrayList<Peptide> notFound = new ArrayList<Peptide>();
    
    /**
     * Creates a new <code>DataMerger</code> object.
     * @param fileName 
     * @param dataFile
     * @param pDPeps
     * @param dbase
     * @param fastaDB 
     * @param dbtype 
     * @param outputDir 
     * @throws IOException 
     * @throws InterruptedException  
     */
    public XGlycScanner(String fileName, 
                        ArrayList<PSMPeptide> pDPeps,
                        //String dataFile, 
                        Database dbase, 
                        FastaDb fastaDB, 
                        SearchDatabaseType dbtype,
                        String outputDir) throws IOException, InterruptedException {
        //this.dataFile = dataFile;
        this.file = fileName;
        scan(pDPeps, dbase, fastaDB, dbtype, outputDir);
    } // end DataMerger( ... ) 


   
    private void scan(ArrayList<PSMPeptide> pDPeps, Database dbase, 
                        FastaDb fastaDB, SearchDatabaseType dbtype, String outputDir) 
            throws IOException, InterruptedException{
        db = fastaDB;
        generatePeptides(pDPeps, dbase, dbtype, outputDir);
        
    } /* end scan() 
    
    


    /**
     * Prints the specified information regarding the given <code>Peptide</code> to the given <code>
     * PrintWriter</code>.
     */
    private void printPep( PrintWriter pw, String prefix, String[] tissues, Peptide p, int format ) {
            
        pw.println( toString(prefix, tissues, p, format) );

    } /* end printPep( ... ) */


    /**
     * Formulates and returns a <code>String</code> representation for the given <code>Peptide</code>.
     */
    private String toString( String prefix, String[] tissues, Peptide p, int format ) {
            
        StringBuilder sb = new StringBuilder();
        // Print out the prefix
        sb.append( prefix ).append( "\t" );

        // Print the peptide information
        sb.append( p.fseq ).append( "\t" );
        sb.append( p.numTrypticEnds ).append( "\t" );
        sb.append( p.evalValue ).append( "\t" );
        sb.append( p.mass ).append( "\t" );
        sb.append( p.getScanNumber() );
        //print c/f values
        /*
         * "Retention time" + "\t" + 
            "m/z [Da]" + "\t" +
            "Charge"  + "\t" +
            "Scan number");
         */
        //sb.append(p.rT).append( "\t" );
        // sb.append(p.mz).append( "\t" );
        //sb.append(p.charge).append( "\t" );
        return sb.toString();

    } /* end toString( ... ) */


    /**
     * Generates the list of peptides to be merged with the existing data file.
     */
    private void generatePeptides(ArrayList<PSMPeptide> pDPeps, Database dbase,
                                    SearchDatabaseType dbtype, String outputDir) 
            throws IOException, InterruptedException {
        
        // Maps a clean sequence to it's peptide object
        HashMap pepMap = new HashMap();

        String tissue = new File(file).getName();
        String source = "JHU";
        pepMap = generatePeptides(source, tissue, pepMap, pDPeps, dbase );

        // Look up each unique peptide in the IPI database
        int pepNum = 1;
        //int totalPepNum = pepMap.values().size();
        //System.out.println( "  Looking up " + totalPepNum + " [Glyco]peptides (not Glycosites) in datafile..." );

        Iterator pepIter = pepMap.values().iterator();
        
        while( pepIter.hasNext() ){
            Peptide p = (Peptide)pepIter.next();
            ipiLookup( p, dbtype );
            pepNum++;
            
        }
        
        // As a good check, output entire datamap
        pepIter = dataMap.keySet().iterator();
        
        //-- added: an output file for dataMap
        //-- filename + ".GMap" i.e file's glycosite Map
        String gMapFolder = outputDir + File.separator + "xGlycscan.tables" + File.separator + "gmaps";
        if(new File(gMapFolder).exists() == false){
            new File(gMapFolder).mkdirs();
        }
        String gMapFile = gMapFolder + File.separator + new File(file).getName() + ".gmap";
        PrintWriter printer = new PrintWriter(gMapFile);
        //print a table header
        printer.println("Accession\t" + 
                            "Location\t" + 
                            "Formatted peptide sequence\t" + 
                            "Number of tryptic ends\t" + 
                            "Evaluation value\t" + 
                            "Estimated mass" + "\t" + 
                             //c/f values
                             //"Retention time" + "\t" + 
                             //"m/z [Da]" + "\t" +
                             //"Charge"  + "\t" +
                             "Scan number");
        while( pepIter.hasNext() ){
            String key = (String)pepIter.next();
            Peptide p = (Peptide)dataMap.get( key );
            StringTokenizer stKey = new StringTokenizer( key, "." );
            String ipi = stKey.nextToken();
            String site = stKey.nextToken();
            String prefix = ipi + "\t" + site;
            printPep( printer, prefix, null, p, TISSUE_NONE );
        }
        printer.flush();
        printer.close();
        

    } /* end generatePeptides() */


    /**
     * Strips out unneeded information from the interact data and loads <code>Peptide</code> objects into a
     * peptide map.
     */
    private HashMap generatePeptides(String source, String tissue, HashMap pepMap,ArrayList<PSMPeptide> pDPeps,
            Database database)
        throws IOException {
        XGSFilePeptidePSMExtractor pepExtractor = new XGSFilePeptidePSMExtractor();
        
        /************************************************************************************** 
         * this step may be omitted in subsequent implementation as XGlycScanner.Peptide may be 
         * directly extracted from the PSMPeptide object
         */
        ArrayList<XGSFilePeptidePSM> peptides = 
                pepExtractor.extractPeptidePSMs(file, pDPeps,database); // serves only to reduce size of object in memory       
        /**************************************************************************************/
        System.out.println( "  Generating XGlycScanner.Peptide object(s) from file " + new File(file).getName());
        int pepNum = 0;        
        Iterator<XGSFilePeptidePSM> itr  = peptides.iterator();
        while(itr.hasNext()){
            XGSFilePeptidePSM idPep = itr.next(); 
            pepNum++;
            String loc = idPep.getFileName();// file - 
                                             // spectrum file which is usually a concatenation of 
                                             // Filename+RT+mz+Charge+Intensity (to ensure spectrum is unique). 
                                             // Synonymously referred to as location, loc in this program 
                                             // [See XGSFilePeptidePSMExtractor.extractPeptidePSMs]
            String rawseq = idPep.getExtendedSequence();        //st.nextToken();	// Peptide
            double prob_val = idPep.getProbability(); 
            // c/f values
            double rT = idPep.getrT();
            double mz = idPep.getMz();
            int charge = idPep.getCharge();
            int scanNumber = idPep.getScanNumber();
            
            
            // Create a clean and formatted sequence from the raw sequence
            String clean_seq = cleanSeq( rawseq );
            String form_seq = formatSeq( rawseq );
            // Prepend the institutional source and filename to the location
            loc = source + "/" + new File(file).getName().concat( loc.replaceAll("[\"|,]","").substring(0) );

            // Create the peptide object
            Peptide p = new Peptide(clean_seq,
                                        form_seq, 
                                            prob_val, 
                                                tissue, 
                                                    loc,
                                                      rT, mz, charge, scanNumber );

            // Add this new peptide to the peptide map, or scan with the existing
            Peptide xp = (Peptide)pepMap.get( clean_seq );
            if( xp != null ) p.merge( xp ); //re-implement merge to rather return values of the better peptide
            pepMap.put( clean_seq, p );

        }
        
        System.out.println("  " + pepMap.size() + " unique clean_peptide_sequence mapped to \"XGlycScanner.Peptide\" object");
        return pepMap;

    } /* end generatePeptides() */


    
    
    

    /**
     * Assigns an ipi to the given <code>Peptide</code>.
     */

    private void ipiLookup( Peptide p, SearchDatabaseType dbtype )
    {
        // Some sequences have wildcards in them
        String p_strseq = p.cseq.replace( 'X', '.' );

        Pattern pseq = Pattern.compile( p_strseq );
        Matcher m = nxts.matcher( p.cseq );
        ArrayList nxtsSites = new ArrayList();
        while( m.find() ) nxtsSites.add( new Integer(m.start()) );
        int[] nxtsOffset = new int[ nxtsSites.size() ];
        for( int i = 0; i < nxtsOffset.length; i++ )
            nxtsOffset[i] = ((Integer)nxtsSites.get(i)).intValue();

        // Lookup the entries in the database containing this peptide sequence
        FastaDbEntry[] entries = db.getEntriesWhoseSequenceContains( p.cseq );

        // Add this peptide to the ipi.nxts map for every entry it was found in. This map will
        // be used during the scan to quickly find relevant peptides.
        String[] ipis = new String[ entries.length ];
        for( int i = 0; i < entries.length; i++ )
        {
            ipis[i] = FastaDb.ipiToString( entries[i].getIpi(),dbtype );

            // Determine the index of the nxts site within the database entry spanned by this peptide
            m = pseq.matcher( entries[i].getSequence() );
            ArrayList hitList = new ArrayList();
            while( m.find() )
            {
                for( int j = 0; j < nxtsOffset.length; j++ )
                    hitList.add( new Integer(m.start()+1+nxtsOffset[j]) );
            }

            // Add this peptide to the data map if it's better than what's already there
            for( int j = hitList.size()-1; j >= 0; --j )
            {
                String key = ipis[i] + "." + ((Integer)hitList.get(j)).intValue();
                Peptide xp = (Peptide)dataMap.get( key );
                if( xp != null )
                {
                    xp.merge( p );
                    p.merge( xp );
                    p = evaluate( p, xp );
                }
                // Note: for peptides that span > 1 nxts site it's important to clone them
                // before placing them in the datamap to avoid accruing tissue data from
                // merges with peptides that match just one of the peptide's nxts sites
                if( j == hitList.size()-1 ) dataMap.put( key, p );
                else dataMap.put( key, new Peptide(p) );
            }

        } // end for every entry

        // Output if not found in the database
        //if( ipis.length == 0  &&  outputOps[OUT_NOT_FOUND] )
        //        outputs[ OUT_NOT_FOUND ].println( p.cseq );
        if(ipis.length == 0){
            notFound.add(p);
        }

    } /* end method ipiLookup( Peptide ) */


    /**
        * Returns the better of the two peptides.
        *
        * Rules:
        * If peptide is new, keep it
        *
        * If peptide matches an existing peptide, compare the number of tryptic ends
        *   if different, keep peptide with more tryptic ends
        *   if same, compare length
        *     if different, keep shorter
        *     if same, keep existing peptide
        */

    private Peptide evaluate( Peptide pep, Peptide xpep )
    {
        Peptide keep = null;
        Peptide toss = null;
        String reason = "";

        // If either peptide is a dummy peptide, return the other (this is the case when a file to be
        // merged doesn't contain the complete peptide information, e.g. a sequence or ipi list).
        if( pep.cseq.equals(DUMMY_SEQ) )
        {
            keep = xpep;
            toss = pep;
            reason = "Incomplete peptide information";
        }
        else if( xpep.cseq.equals(DUMMY_SEQ) )
        {
            keep = pep;
            toss = xpep;
            reason = "Incomplete peptide information";
        }

        // If either peptide has a proline after the tryptic end, return the other
        if( pep.numProlineTrypticEnds > xpep.numProlineTrypticEnds )
        {
            keep = xpep;
            toss = pep;
            reason = "proline tryptic ends";
        }
        else if( xpep.numProlineTrypticEnds > pep.numProlineTrypticEnds )
        {
            keep = pep;
            toss = xpep;
            reason = "proline tryptic ends";
        }
        else
        {
            // If the new peptide has more tryptic ends than the existing match, keep the new peptide
            // over the existing one
            if( pep.numTrypticEnds > xpep.numTrypticEnds )
            {
                keep = pep;
                toss = xpep;
                reason = "tryptic ends";
            }

            // Conversely, if the existing peptide has more tryptic ends, keep it over the new peptide
            else if( pep.numTrypticEnds < xpep.numTrypticEnds )
            {
                keep = xpep;
                toss = pep;
                reason = "tryptic ends";
            }

            // Otherwise, if they have the same number of tryptic ends, compare their lengths
            else
            {
                int pep_len = pep.cseq.length();
                int xpep_len = xpep.cseq.length();

                // Keep the shorter of the two peptides
                if( pep_len < xpep_len )
                {
                    keep = pep;
                    toss = xpep;
                    reason = "peptide length";
                }
                // ... or if they are the same length, keep the peptide with the higher probability
                else
                {
                    if( pep.evalValue > xpep.evalValue )
                    {
                        keep = pep;
                        toss = xpep;
                        reason = "Evaluation value";
                    }
                    else if( xpep.evalValue > pep.evalValue )
                    {
                        keep = xpep;
                        toss = pep;
                        reason = "Evaluation value";
                    }
                    else
                    {
                        keep = xpep;
                        toss = pep;
                        reason = "Evaluation value";
                    }
                }
            }

        } // end else neither peptides have prolines

        keep.add2Tossed(toss);
        return keep;
    } /* end evaluate( Peptide, Peptide, boolean ) */


    /**
        * Converts the raw sequence string to a clean sequence with only amino acid characters.
        */

    private String cleanSeq( String rawseq )
    {
        return rawseq.replaceAll( crap, "" );
    }


    /**
     * Formats the raw sequence such that only the NXT/S motif is denoted by a '#', and the tryptic
     * ends by '.'s.
     */

    @SuppressWarnings({"RedundantStringConstructorCall", "ManualArrayToCollectionCopy"})
    private String formatSeq(String rawseq){
        int dash = 0;
        if( rawseq.startsWith("-") ) dash = -1; else if( rawseq.endsWith("-") ) dash = 1;
        String clean_seq = cleanSeq( rawseq );

        StringBuffer newSeq = new StringBuffer();
        Matcher m = nxts.matcher( clean_seq );

        while( m.find() )
        {
            // Ignore NXT/S sites at the start of the sequence
            if( m.start() == 0 ) continue;

            // Otherwise add a '#' to the 'N'
            String nxtsSite = m.group();
            char[] newNxtsSite = new char[] {
                                                nxtsSite.charAt( 0 ), '#',
                                                nxtsSite.charAt( 1 ),
                                                nxtsSite.charAt( 2 )
                                            };
            m.appendReplacement( newSeq, new String(newNxtsSite) );
        }
        m.appendTail( newSeq );

        // Replace the '.'s
        String formseq = newSeq.toString();
        if( dash == -1 ) formseq = new String("-" + formseq); else if( dash == 1 ) formseq = new String(formseq + "-" );

        char[] cseq = formseq.toCharArray();
        char[] cseqFormatted = new char[ cseq.length + 2 ];
        cseqFormatted[0] = cseq[0];
        cseqFormatted[1] = '.';
        for( int j = 1; j < cseq.length-1; j++ ) cseqFormatted[j+1] = cseq[j];
        cseqFormatted[cseq.length] = '.';
        cseqFormatted[cseqFormatted.length-1] = cseq[cseq.length-1];

        formseq = new String( cseqFormatted );
        return formseq;

    } /* end formatSeq( String ) */


   


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * \
     *																										*
     * 		Inner Classes																					*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Encapsulates relevant information about a peptide fragment.
     */

    public static class Peptide
    {
        protected String cseq;
        protected String fseq;
        protected double evalValue;
        protected double mass;
        protected int numTrypticEnds = 0;
        protected int numProlineTrypticEnds = 0;
        
        //c/f attributes
        protected double rT;
        protected double mz;
        protected int charge;
        protected int scanNumber;
        
        // tissue and file_location map... (old implementation)
        protected HashMap tissueMap = new HashMap();
        protected ArrayList<Peptide> tossed = new ArrayList<Peptide>();

        /**
            * Creates a new <code>Peptide</code>.
            *
            * @param cseq the clean peptide sequence
            * @param fseq the formatted peptide sequence
            * @param prob  
            */
        protected Peptide( String cseq, String fseq, double prob )
        {
            this.cseq = cseq;
            this.fseq = fseq;
            this.evalValue = prob;

            // Determine the number of tryptic ends on this peptides
            String smallseq = fseq.toLowerCase();
            int start = 0;
            int end = smallseq.length() - 3;
            if( (smallseq.charAt(start) == 'r')  ||
                    (smallseq.charAt(start) == 'k')  ||
                    (smallseq.charAt(start) == '-')   ) numTrypticEnds++;
            if( (smallseq.charAt(end) == 'r')  ||
                    (smallseq.charAt(end) == 'k')  ||
                    (smallseq.charAt(end+2) == '-') ) numTrypticEnds++;

            if( smallseq.startsWith("r.p") ) numProlineTrypticEnds++;
            if( smallseq.startsWith("k.p") ) numProlineTrypticEnds++;
            if( smallseq.endsWith("r.p") ) numProlineTrypticEnds++;
            if( smallseq.endsWith("k.p") ) numProlineTrypticEnds++;

            mass = calculateMass(cseq);
        }

        /**
            * Creates a new <code>Peptide</code>.
            * @param cseq 
            * @param fseq
            * @param prob
            * @param file
            * @param tissue  
            */
        protected Peptide( String cseq, String fseq, double prob, String tissue, String file )
        {
            this( cseq, fseq, prob );

            // Add the tissue/file location
            HashSet locSet = new HashSet();
            locSet.add( file );
            tissueMap.put( tissue, locSet );
        }
        
        protected Peptide( String cseq, String fseq, 
                                double prob, String tissue, String file,
                                    double rTime, double m2charge, int ch, int scanN)
        {
            this( cseq, fseq, prob );

            // Add the tissue/file location
            HashSet locSet = new HashSet();
            locSet.add( file );
            tissueMap.put( tissue, locSet );
            rT = rTime;
            mz = m2charge;
            charge = ch;
            scanNumber = scanN;
            
        }

        /**
            * Creates a new <code>Peptide</code>.
            * @param cseq 
            * @param tissueMap
            * @param fseq 
            * @param prob
            * @param rTime
            * @param m2charge
            * @param ch
            * @param scanN  
            */
        protected Peptide( String cseq, String fseq, double prob, HashMap tissueMap, 
                              double rTime, double m2charge, int ch, int scanN)
        {
            this( cseq, fseq, prob );

            // Deep clone the tissue map
            this.tissueMap = new HashMap( tissueMap.size() );
            Iterator tissIter = tissueMap.keySet().iterator();
            while( tissIter.hasNext() ) {
                String tissue = (String)tissIter.next();
                HashSet tissList = (HashSet)tissueMap.get( tissue );
                this.tissueMap.put( tissue, (HashSet)tissList.clone() );
            }
            //
            rT = rTime;
            mz = m2charge;
            charge = ch;
            scanNumber = scanN;
        }

        /**
            * Creates a replica of the given <code>Peptide</code>.
            * @param p 
            */
        protected Peptide( Peptide p )
        {
            this( p.cseq, p.fseq, p.evalValue, p.tissueMap, p.rT, p.mz, p.charge, p.scanNumber );
            
            
        }

        /**
            * Returns <code>true</code> if this <code>Peptide</code> equals the given <code>Object</code>.
            */
        @Override
        public boolean equals( Object obj )
        {
            try
            {
                Peptide pep = (Peptide)obj;
                return cseq.equals( pep.cseq );
            }
            catch( ClassCastException CCE )
            {
                return false;
            }
        }

        /**
            * Returns the number of times this <code>Peptide</code> has been observed.
            * @return 
            */
        public int getNumHits()
        {
            int hits = 0;
            Iterator tissIter = tissueMap.keySet().iterator();
            while( tissIter.hasNext() ) {
                String tissue = (String)tissIter.next();
                HashSet tissList = (HashSet)tissueMap.get( tissue );
                hits += tissList.size();
            }
            return hits;
        }

        /**
         * Returns the set of all unique, institutional sources that have contributed to the identification
         * of this <code>Peptide</code>.  This information is embedded within the first token of each tissue
         * location string and is returned as an array of 2-element string arrays, where the first element
         * is the tissue and the second element is the source.
         * @return 
         */
        public String[][] getSources()
        {
            HashMap sourceMap = new HashMap();
            int key_val_pairs = 0;
            Iterator tissIter = tissueMap.keySet().iterator();
            while( tissIter.hasNext() ) {
                String tissue = (String)tissIter.next();
                HashSet tissList = (HashSet)tissueMap.get( tissue );
                HashSet sourceSet = new HashSet();
                Iterator i = tissList.iterator();
                while( i .hasNext() ) {
                    String hit = (String)i.next();
                    String source = hit.substring( 0, hit.indexOf('/') );
                    sourceSet.add( source );
                }
                if( sourceSet.size() > 0 ) sourceMap.put( tissue, sourceSet );
                key_val_pairs += sourceSet.size();
            }

            String[][] sources = new String[ key_val_pairs ][2];
            tissIter = sourceMap.keySet().iterator();
            while( tissIter.hasNext() ) {
                key_val_pairs--;
                String tissue = (String)tissIter.next();
                HashSet sourceSet = (HashSet)sourceMap.get( tissue );
                Iterator i = sourceSet.iterator();
                while( i.hasNext() ) {
                    String source = (String)i.next();
                    sources[ key_val_pairs ][0] = tissue;
                    sources[ key_val_pairs ][1] = source;
                }
            }

            return sources;
        }

        /**
         * Merges this <code>Peptide</code> with the given <code>Peptide</code> by keeping the better of the
         * two probabilities and concatenating tissue information.
         */
        private void merge( Peptide p ){
            // Take the better of the two probabilities
            //this.evalValue = Math.max( this.evalValue, p.evalValue );
            //rather assign the better values to this.Object
            if(p.getEvalValue() > this.evalValue){
                this.evalValue = p.getEvalValue();
                this.rT = p.getrT();
                this.mz = p.getMz();
                this.charge = p.getCharge();
                this.scanNumber = p.getScanNumber();
            }    
            
            
            
            Set tissSet = p.tissueMap.keySet();
            String[] tissues = new String[ tissSet.size() ];
            tissSet.toArray( tissues );
            for( int i = 0; i < tissues.length; i++ ){
                HashSet locSet = (HashSet)p.tissueMap.get( tissues[i] );

                HashSet xLocSet = (HashSet)this.tissueMap.get( tissues[i] );
                if( xLocSet != null ) xLocSet.addAll( locSet );
                else xLocSet = locSet;
                if( xLocSet == null ) xLocSet = new HashSet();

                tissueMap.put( tissues[i], xLocSet );
            }

        } /* end scan( Peptide ) */
        
        
        /**
         * 
         * @param p
         */
        protected void add2Tossed( Peptide p ){
            tossed.add( p );
        }
        
        /**
         * 
         * @return
         */
        public ArrayList<Peptide> getTossed(){
            return tossed;
        } 
        
        /**
         * 
         * @return
         */
        public String getCleanSequence(){
            return cseq;
        }
        
        /**
         * 
         * @return
         */
        public String getFormattedSequence(){
            return fseq;
        }

        /**
         * 
         * @return
         */
        public double getMass() {
            return mass;
        }

        /**
         * 
         * @return
         */
        public int getNumProlineTrypticEnds() {
            return numProlineTrypticEnds;
        }

        /**
         * 
         * @return
         */
        public int getNumTrypticEnds() {
            return numTrypticEnds;
        }

        /**
         * 
         * @return
         */
        public double getEvalValue() {
            return evalValue;
        }

        /**
         * 
         * @return
         */
        public HashMap getTissueMap() {
            return tissueMap;
        }

        public int getCharge() {
            return charge;
        }

        public double getMz() {
            return mz;
        }

        public double getrT() {
            return rT;
        }

        public int getScanNumber() {
            return scanNumber;
        }
        
        
        
        

    } /* end class Peptide */



    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
     *																										*
     * 		Mass Calculation																				*
     *																										*
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*
     * The following code is all for calculating peptide masses.
     */
    private static double calculateMass( String str_seq )
    {
        char[] seq = str_seq.toCharArray();
        double mass = 0.0;
        for( int i = 1; i < seq.length-1; i++ )
        {
            mass += AAMassTable[ charToInt[ seq[i] ] ];
        }
        mass += 18.0;
        return mass;
    }


    private static int[] charToInt;
    static {
        charToInt = new int[ 91 ];
        for( int i = 0; i < charToInt.length; i++ ) charToInt[i] = -1;
        charToInt[ (int)'A' ] = 0;
        charToInt[ (int)'C' ] = 1;
        charToInt[ (int)'D' ] = 2;
        charToInt[ (int)'E' ] = 3;
        charToInt[ (int)'F' ] = 4;
        charToInt[ (int)'G' ] = 5;
        charToInt[ (int)'H' ] = 6;
        charToInt[ (int)'I' ] = 7;
        charToInt[ (int)'K' ] = 8;
        charToInt[ (int)'L' ] = 9;
        charToInt[ (int)'M' ] = 10;
        charToInt[ (int)'N' ] = 11;
        charToInt[ (int)'P' ] = 12;
        charToInt[ (int)'Q' ] = 13;
        charToInt[ (int)'R' ] = 14;
        charToInt[ (int)'S' ] = 15;
        charToInt[ (int)'T' ] = 16;
        charToInt[ (int)'V' ] = 17;
        charToInt[ (int)'W' ] = 18;
        charToInt[ (int)'Y' ] = 19;
        charToInt[ (int)'.' ] = 20;
        charToInt[ (int)'*' ] = 21;
        charToInt[ (int)'#' ] = 22;
        charToInt[ (int)'X' ] = 23;
    }

    private static double[] AAMassTable = {
                                            71.037114,	// A;
                                            103.00919,	// C;
                                            115.02694,	// D;
                                            129.04259,	// E;
                                            147.06841,	// F;
                                            57.021464,	// G;
                                            137.05891,	// H;
                                            113.08406,	// I;
                                            128.09496,	// K;
                                            113.08406,	// L;
                                            131.04048,	// M;
                                            114.04293,	// N;
                                            97.052764,	// P;
                                            128.05858,	// Q;
                                            156.10111,	// R;
                                            87.032029,	// S;
                                            101.04768,	// T;
                                            99.068414,	// V;
                                            186.07931,	// W;
                                            163.06333,	// Y;
                                            0.0,		// .;
                                            16.0,		// *;
                                            0.987,		// #;
                                            118.80571	// X;
                                        };

    /**
     * 
     * @return
     */
    public HashMap getDataMap() {
        return dataMap;
    }

    /**
     * 
     * @return
     */
    public ArrayList<Peptide> getNotFound() {
        return notFound;
    }
    
    

}